<?php

namespace AppBundle\Form;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

/**
 * Class ContactType
 * @package AppBundle\Form
 */
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'label' => 'Name'
                ]
            )
            ->add(
                'vorname',
                'text',
                [
                    'label' => 'Vorname'
                ]
            )
            ->add(
                'email',
                'email',
                [
                    'label' => 'E-Mail'
                ]
            )
            ->add(
                'phone',
                'text',
                [
                    'label' => 'Telefon'
                ]
            )
            ->add(
                'stoecke',
                'text',
                [
                    'required'=>false,
                    'label' => 'Ich möchte'
                ]
            )
            ->add(
                'friends',
                'text',
                [
                    'required'=>false,
                    'label' => 'Ich bringe'
                ]
            )
            ->add(
                'announce',
                'checkbox',
                [
                    'required'=>false,
                    'label' => 'Ich bringe'
                ]
            )
            ->add(
                'message',
                'textarea',
                array(
                    'label' =>'Deine Nachricht',
                    'attr' => array(
                        'cols'        => 90,
                        'rows'        => 10,
                        'placeholder' => ''
                    )
                )
            )
            ->add(
                'recaptcha',
                EWZRecaptchaType::class
            );

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $collectionConstraint = new Collection(
            array(
                'name'    => array(
                    new NotBlank(array('message' => 'Name darf nicht leer sein.')),
                    new Length(array('min' => 2))
                ),
                'vorname'    => array(
                    new NotBlank(array('message' => 'Vorname darf nicht leer sein.')),
                    new Length(array('min' => 2))
                ),
                'email'   => array(
                    new NotBlank(array('message' => 'Email darf nicht leer sein.')),
                    new Email(array('message' => 'Invalide Email Adressen.'))
                ),
                'phone'   => array(
                    new NotBlank(array('message' => 'Telefonnummer darf nicht leer sein.')),
                    new Length(array('min' => 2))
                ),
                'stoecke'   => array(
                ),
                'friends'   => array(
                ),
                'announce' => [],
                'recaptcha' => [
                    new RecaptchaTrue(['message' => 'Bitte setze das Häkchen.'])
                ],
                'message' => array(
                    new NotBlank(array('message' => 'Nachricht darf nicht leer sein.')),
                    new Length(array('min' => 20))
                )
            )
        );

        $resolver->setDefaults(
            array(
                'constraints' => $collectionConstraint
            )
        );
    }

    public function getName()
    {
        return 'contact';
    }

}