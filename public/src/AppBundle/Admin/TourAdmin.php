<?php

namespace AppBundle\Admin;

use Doctrine\Common\Collections\Collection;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TourAdmin extends AbstractAdmin
{
    protected $datagridValues = array(

        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titel')
            ->add('datum')
            ->add('uhrzeit')
            ->add('treffpunkt')
            ->add('parkmoeglichkeit')
            ->add('wanderweg')
            ->add('region')
            ->add('laenge')
            ->add('dauer')
            ->add('bodenbeschaffenheit')
            ->add('besonderheiten')
            ->add('schwierigkeit')
            ->add('teilnahmegebuehr')
            ->add('maxTeilnehmer')
            ->add('hidden');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titel')
            ->add('datum')
//            ->add('uhrzeit')
//            ->add('treffpunkt')
//            ->add('parkmoeglichkeit')
//            ->add('wanderweg')
//            ->add('region')
//            ->add('laenge')
//            ->add('dauer')
//            ->add('bodenbeschaffenheit')
//            ->add('besonderheiten')
//            ->add('schwierigkeit')
//            ->add('teilnahmegebuehr')
//            ->add('maxTeilnehmer')
            ->add('hidden')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show'   => array(),
                        'edit'   => array(),
                        'delete' => array(),
                    )
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $tour=$formMapper->getAdmin()->getSubject();
        $url = null;
        if($tour && $tour->getId()){
            $url = '/touren_vorschau/'.$tour->getId();
        }
        $formMapper
            ->add('titel',null,['help'=>$url?'<a href="'.$url.'" target="_blank">Vorschau</a>':''])
            ->add('vorschautext', 'textarea')
            ->add('datum')
            ->add('uhrzeit')
            ->add('treffpunkt', 'textarea')
            ->add('parkmoeglichkeit', 'textarea')
            ->add('wanderweg', 'textarea')
            ->add('region','textarea')
            ->add('laenge')
            ->add('dauer')
            ->add('bodenbeschaffenheit')
            ->add('besonderheiten')
            ->add('schwierigkeit')
            ->add('teilnahmegebuehr')
            ->add('maxTeilnehmer')
            ->add('hidden')
            ->add(
                'images',
                'sonata_type_collection',
                array(
                    'by_reference' => false,
                ),
                array(
                    'edit'     => 'inline',
                    'inline'   => 'table',
                )
            );
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titel')
            ->add('datum')
            ->add('uhrzeit')
            ->add('treffpunkt')
            ->add('parkmoeglichkeit')
            ->add('wanderweg')
            ->add('region')
            ->add('laenge')
            ->add('dauer')
            ->add('bodenbeschaffenheit')
            ->add('besonderheiten')
            ->add('schwierigkeit')
            ->add('teilnahmegebuehr')
            ->add('maxTeilnehmer')
            ->add('hidden');
    }


}
