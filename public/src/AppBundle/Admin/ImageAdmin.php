<?php

namespace AppBundle\Admin;
use AppBundle\Entity\Image;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class ImageAdmin
 * @package AppBundle\Admin
 */
class ImageAdmin extends AbstractAdmin
{
    protected $datagridValues = array(

        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'sortierung',
    );
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('titel')
            ->add('filename')
            ->add('savepath')
            ->add('sortierung')
            ->add('parkmoeglichkeit',null,['label'=>'Vorschau'])
            ->add('treffpunkt')
            ->add('bildrotation')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {


        $listMapper
            ->addIdentifier('titel')
            ->add('tour.titel')
            ->add('filename')
            ->add('sortierung')
            ->add('parkmoeglichkeit',null,['label'=>'Vorschau'])
            ->add('treffpunkt')
            ->add('bildrotation')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('id', null, ['disabled'=>true])
            ->add('sortierung', null, ['required'=>false])
            ->add('filename',null, ['disabled'=>true, 'label'=>'bild', 'required'=>false])
            ->add('titel', null, ['required'=>false])
            ->add('parkmoeglichkeit',null,['label'=>'Vorschau'])
            ->add('treffpunkt',null,['label'=>'Treff'])
            ->add('bildrotation',null,['label'=>'Rotation'])
            ->add('tmpImg','file',['label'=>'Upload', 'required'=>false])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('titel')
            ->add('filename')
            ->add('savepath')
            ->add('sortierung')
            ->add('parkmoeglichkeit',null,['label'=>'Vorschau'])
            ->add('treffpunkt')
            ->add('bildrotation')
        ;
    }

//    /**
//     * @param Image $image
//     */
//    public function saveFile(Image $image)
//    {
//        $tmpImage = $image->getTmpImg();
//        if ($tmpImage && $tmpImage->getFilename()) {
//            $tmpImage->move(
//                'upload',
//                $tmpImage->getClientOriginalName()
//            );
//            $image->setFilename($tmpImage->getClientOriginalName());
//            $image->setSavepath('upload/');
//        }
//    }
//
//    /**
//     * @param Image $image
//     */
//    public function renameFile(Image $image){
//        if (strlen($image->getFilename()) && file_exists('upload/'.$image->getFilename())) {
//            rename('upload/'.$image->getFilename(),'upload/'.$image->getId());
//        }
//    }
//
//    /**
//     * @param Image $image
//     */
//    public function prePersist($image)
//    {
//        $this->saveFile($image);
//    }
//
//    /**
//     * @param Image $image
//     */
//    public function postPersist($image)
//    {
//        $this->renameFile($image);
//    }
//
//    /**
//     * @param Image $image
//     */
//    public function postUpdate($image)
//    {
//        $this->renameFile($image);
//    }
//
//    /**
//     * @param Image $image
//     */
//    public function preUpdate($image)
//    {
//        $this->saveFile($image);
//    }


}
