<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Tour;
use AppBundle\Form\ContactType;
use AppBundle\Form\RegistrationType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('AppBundle:Default:index.html.twig', []);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/aktuelle_tour", name="current_tour")
     */
    public function currentTourAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->get('doctrine')->getManager();
        $qb = $em->createQueryBuilder();

        $q = $qb->select('t')
            ->from('AppBundle:Tour', 't')
            ->where(
                $qb->expr()->gt('t.datum', ':now')
            )
            ->setParameter('now', new \DateTime())
            ->andWhere('t.hidden = :hidden ')
            ->setParameter('hidden', 0)
            ->orderBy('t.datum', 'DESC')
            ->getQuery();

        $result = $q->getResult();
        if (count($result)) {
            /** @var Tour $tour */
            $tour = $result[0];
            $form = $this->createForm(new RegistrationType());

            if ($request->isMethod('POST')) {
                $form->bind($request);

                if ($form->isValid()) {
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Anmeldung Tour GLW')
                        ->setFrom('rsmu@gmx.de')
                        ->setTo($this->getParameter('mail_address_contact'))
                        ->setBody(
                            $this->renderView(
                                '@App/Default/mail.html.twig',
                                array(
                                    'name'       => $form->get('name')->getData(),
                                    'vorname'    => $form->get('vorname')->getData(),
                                    'email'      => $form->get('email')->getData(),
                                    'phone'      => $form->get('phone')->getData(),
                                    'announce'   => $form->get('announce')->getData(),
                                    'stoecke'    => $form->get('stoecke')->getData(),
                                    'friends'    => $form->get('friends')->getData(),
                                    'message'    => $form->get('message')->getData(),
                                    'isRegister' => 1

                                )
                            ),
                            'text/html'
                        );

                    $send = $this->get('mailer')->send($message);

                    $request->getSession()->getFlashBag()->add(
                        'success',
                        'Die Anmeldung wurde versendet. Vielen Dank!!'
                    );

                    return $this->redirect($this->generateUrl('current_tour'));
                }
            }

            return $this->render(
                'AppBundle:Default:currentTour.html.twig',
                ['tour' => $tour, 'form' => $form->createView()]
            );
        } else {
            return $this->render('AppBundle:Default:noCurrentTour.html.twig', array());
        }

    }
    /**
     * @param Request $request
     * @return Response
     * @Route("/touren_vorschau", name="preview_tours")
     */
    public function previewToursAction(Request $request) {
        /** @var EntityManager $em */
        $em = $this->get('doctrine')->getManager();
        $qb = $em->createQueryBuilder();

        $q = $qb->select('t')
            ->from('AppBundle:Tour', 't')
            ->where(
                $qb->expr()->gt('t.datum', ':now')
            )
            ->setParameter('now', new \DateTime())
            ->andWhere('t.hidden = :hidden ')
            ->setParameter('hidden', 0)
            ->orderBy('t.datum', 'ASC')
            ->getQuery();

        $tours = $q->getResult();
        return $this->render(
            'AppBundle:Default:previewTours.html.twig',
            ['tours' => $tours]
        );

    }

    /**
     * @param Request $request
     * @param int id
     * @return Response
     * @Route("/touren_vorschau/{id}", name="tour_preview_detail")
     */
    public function tourPreviewDetailAction(Request $request, $id = null)
    {
        $em = $this->get('doctrine')->getManager();
        /** @var Tour $tour */
        $tour = $em->find('AppBundle:Tour', $id);

        return $this->render('AppBundle:Default:tourPreviewDetail.html.twig', ['tour' => $tour]);

    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/kurse", name="lessons")
     */
    public function lessonsAction(Request $request)
    {
        return $this->render('AppBundle:Default:lessons.html.twig');


    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/tourenarchiv", name="tour_archive")
     */
    public function tourArchiveAction(Request $request)
    {

        $em = $this->get('doctrine')->getManager();
        $qb = $em->getRepository('AppBundle:Tour')->createQueryBuilder('t');

        $qb->select('t')
            ->where('t.datum < CURRENT_DATE()')
            ->andWhere('t.hidden = 0')
            ->orderBy('t.datum', 'desc');

        return $this->render('AppBundle:Default:tourArchive.html.twig', ['tours' => $qb->getQuery()->getResult()]);

    }

    /**
     * @param Request $request
     * @param int id
     * @return Response
     * @Route("/tourenarchiv/{id}", name="tour_archive_detail")
     */
    public function tourArchiveDetailAction(Request $request, $id = null)
    {
        $em = $this->get('doctrine')->getManager();
        /** @var Tour $tour */
        $tour = $em->find('AppBundle:Tour', $id);

        return $this->render('AppBundle:Default:tourArchivDetail.html.twig', ['tour' => $tour]);

    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/anfrage", name="requests")
     */
    public function requestsAction(Request $request)
    {

        $form = $this->createForm(new ContactType());

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Kontaktanfrage GLW')
                    ->setFrom('rsmu@gmx.de')
                    ->setTo($this->getParameter('mail_address_contact'))
                    ->setTo($this->getParameter('mail_address_contact'))
                    ->setBody(
                        $this->renderView(
                            '@App/Default/mail.html.twig',
                            array(
                                'name'       => $form->get('name')->getData(),
                                'email'      => $form->get('email')->getData(),
                                'phone'      => $form->get('phone')->getData(),
                                'announce'   => $form->get('announce')->getData(),
                                'message'    => $form->get('message')->getData(),
                                'isRegister' => 0

                            )
                        ),
                        'text/html'
                    );

                $send = $this->get('mailer')->send($message);

                $request->getSession()->getFlashBag()->add('success', 'Die Nachricht wurde versendet. Vielen Dank!!');

                return $this->redirect($this->generateUrl('requests'));
            }
        }


        return $this->render('AppBundle:Default:requests.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/impressum", name="imprint")
     */
    public function imprintAction(Request $request)
    {
        return $this->render('AppBundle:Default:imprint.html.twig', []);

    }

    public function menuAction($page)
    {
        return $this->render('AppBundle:Default:menu.html.twig', ['page' => $page]);
    }

    public function footerMenuAction($page)
    {
        return $this->render('AppBundle:Default:footerMenu.html.twig', ['page' => $page]);
    }

    public function sidebarSlideshowAction()
    {
        $em = $this->get('doctrine')->getManager();
        $qb = $em->getRepository('AppBundle:Image')->createQueryBuilder('i');

        $qb->select('i')
            ->join('i.tour', 't')
            ->where('i.bildrotation = 1')
            ->andWhere('t.hidden = 0')
            ->orderBy('t.id', 'desc');

        return $this->render(
            'AppBundle:Default:sidebarSlideshow.html.twig',
            ['images' => $qb->getQuery()->setMaxResults(40)->execute()]
        );
    }

    /**
     * @param int $id
     * @return Response
     * @Route("/image/{id}", name="image")
     */
    public function imageAction($id) {
        $em = $this->get('doctrine')->getManager();
        /** @var Image $image */
        $image = $em->find('AppBundle:Image', $id);
        if ($image) {
            $imagine = $this->container->get('liip_imagine.controller');

            return $imagine->filterAction(
                    $this->get('request_stack')->getCurrentRequest(),
                    $image->getFullFilename(),
                    'thumb_list'
                );
        }else{
            throw new NotFoundHttpException();
        }
    }

}
