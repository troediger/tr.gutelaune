<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\PostPersist;
use Doctrine\ORM\Mapping\PostUpdate;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * Image
 *
 * @ORM\Table(name="symfony_image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 * @Entity @HasLifecycleCallbacks
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titel", type="string" , nullable=true)
     */
    protected $titel="";

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string" )
     */
    protected $filename="";

    /**
     * @var string
     *
     * @ORM\Column(name="savepath", type="string" )
     */
    protected $savepath="";

    /**
     * @var integer
     *
     * @ORM\Column(name="sortierung", type="integer", nullable=true )
     */
    protected $sortierung=0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="parkmoeglichkeit", type="boolean", nullable=true )
     */
    protected $parkmoeglichkeit=false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="treffpunkt", type="boolean", nullable=true )
     */
    protected $treffpunkt=false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bildrotation", type="boolean", nullable=true )
     */
    protected $bildrotation=false;

    /**
     * @var Tour
     * @ORM\ManyToOne(targetEntity="Tour", inversedBy="images", cascade={"persist"})
     * @ORM\JoinColumn(name="tour_id", referencedColumnName="id")
     */
    protected $tour;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    protected $tmpImg;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set tour
     *
     * @param \AppBundle\Entity\Tour $tour
     *
     * @return Image
     */
    public function setTour(\AppBundle\Entity\Tour $tour = null)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return \AppBundle\Entity\Tour
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * Set titel
     *
     * @param string $titel
     *
     * @return Image
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;

        return $this;
    }

    /**
     * Get titel
     *
     * @return string
     */
    public function getTitel()
    {
        return $this->titel;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Image
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set savepath
     *
     * @param string $savepath
     *
     * @return Image
     */
    public function setSavepath($savepath)
    {
        $this->savepath = $savepath;

        return $this;
    }

    /**
     * Get savepath
     *
     * @return string
     */
    public function getSavepath()
    {
        return $this->savepath;
    }

    /**
     * Set sortierung
     *
     * @param integer $sortierung
     *
     * @return Image
     */
    public function setSortierung($sortierung)
    {
        $this->sortierung = $sortierung;

        return $this;
    }

    public function getFullFilename()
    {
        return $this->getSavepath().$this->getId();
    }

    /**
     * Get sortierung
     *
     * @return integer
     */
    public function getSortierung()
    {
        return $this->sortierung;
    }

    /**
     * Set parkmoeglichkeit
     *
     * @param boolean $parkmoeglichkeit
     *
     * @return Image
     */
    public function setParkmoeglichkeit($parkmoeglichkeit)
    {
        $this->parkmoeglichkeit = $parkmoeglichkeit;

        return $this;
    }

    /**
     * Get parkmoeglichkeit
     *
     * @return boolean
     */
    public function getParkmoeglichkeit()
    {
        return $this->parkmoeglichkeit;
    }

    /**
     * Set treffpunkt
     *
     * @param boolean $treffpunkt
     *
     * @return Image
     */
    public function setTreffpunkt($treffpunkt)
    {
        $this->treffpunkt = $treffpunkt;

        return $this;
    }

    /**
     * Get treffpunkt
     *
     * @return boolean
     */
    public function getTreffpunkt()
    {
        return $this->treffpunkt;
    }

    /**
     * Set bildrotation
     *
     * @param boolean $bildrotation
     *
     * @return Image
     */
    public function setBildrotation($bildrotation)
    {
        $this->bildrotation = $bildrotation;

        return $this;
    }

    /**
     * Get bildrotation
     *
     * @return boolean
     */
    public function getBildrotation()
    {
        return $this->bildrotation;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $tmpImg
     */
    public function setTmpImg($tmpImg)
    {
        $this->tmpImg = $tmpImg;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getTmpImg()
    {
        return $this->tmpImg;
    }

    public function saveFile()
    {
        if ($this->getTmpImg() && $this->getTmpImg()->getFilename()) {
            $this->getTmpImg()->move(
                'upload',
                $this->getTmpImg()->getClientOriginalName()
            );
            $this->setFilename($this->getTmpImg()->getClientOriginalName());
            $this->setSavepath('upload/');
        }
    }

    public function renameFile(){
        if (strlen($this->getFilename()) && file_exists('upload/'.$this->getFilename())) {
            rename('upload/'.$this->getFilename(),'upload/'.$this->getId());
        }
    }
    /** @PrePersist */
    public function prePersist()
    {
        $this->saveFile();
        $count = $this->getTour()->getImages()->count();
        $this->setSortierung($count);
    }


    /** @PostPersist */
    public function postPersist()
    {
        $this->renameFile();
    }


    /** @PreUpdate */
    public function preUpdate()
    {
        $this->saveFile();
    }
    /** @PostUpdate */
    public function postUpdate()
    {
        $this->renameFile();
    }
    public function __toString()
    {
        return $this->getTitel() ? : '-';
    }
}
