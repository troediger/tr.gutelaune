<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Tour
 *
 * @ORM\Table(name="symfony_tour")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TourRepository")
 */
class Tour
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="titel", type="string" )
     */
    protected $titel;

    /**
     * @var string
     *
     * @ORM\Column(name="vorschautext", type="text" )
     */
    protected $vorschautext;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="date" )
     */
    protected $datum;


    /**
     * @var string
     *
     * @ORM\Column(name="uhrzeit", type="string" )
     */
    protected $uhrzeit;


    /**
     * @var string
     *
     * @ORM\Column(name="treffpunkt", type="string" )
     */
    protected $treffpunkt;


    /**
     * @var string
     *
     * @ORM\Column(name="parkmoeglichkeit", type="string" )
     */
    protected $parkmoeglichkeit="";


    /**
     * @var string
     *
     * @ORM\Column(name="wanderweg", type="string" )
     */
    protected $wanderweg="";

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string" )
     */
    protected $region="";


    /**
     * @var string
     *
     * @ORM\Column(name="laenge", type="string" )
     */
    protected $laenge="";

    /**
     * @var string
     *
     * @ORM\Column(name="dauer", type="string" )
     */
    protected $dauer="";

    /**
     * @var string
     *
     * @ORM\Column(name="bodenbeschaffenheit", type="string" )
     */
    protected $bodenbeschaffenheit="";

    /**
     * @var string
     *
     * @ORM\Column(name="besonderheiten", type="string" )
     */
    protected $besonderheiten="";


    /**
     * @var string
     *
     * @ORM\Column(name="schwierigkeit", type="string" )
     */
    protected $schwierigkeit="";

    /**
     * @var string
     *
     * @ORM\Column(name="teilnahmegebuehr", type="string" )
     */
    protected $teilnahmegebuehr="";

    /**
     * @var string
     *
     * @ORM\Column(name="max_teilnehmer", type="string" )
     */
    protected $maxTeilnehmer="";

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean" )
     */
    protected $hidden;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="tour",cascade={"all"}, orphanRemoval=true)
     * @OrderBy({"sortierung" = "ASC"})
     */
    private $images;


    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitel() ? : '-';
    }

    /**
     * @return string
     */
    public function getTitel()
    {
        return $this->titel;
    }

    /**
     * @param string $titel
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;

        return $this;
    }

    /**
     * @return string
     */
    public function getVorschautext() {
        return $this->vorschautext;
    }

    /**
     * @param string $vorschautext
     */
    public function setVorschautext($vorschautext) {
        $this->vorschautext = $vorschautext;

        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param \DateTime $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * @return string
     */
    public function getUhrzeit()
    {
        return $this->uhrzeit;
    }

    /**
     * @param string $uhrzeit
     */
    public function setUhrzeit($uhrzeit)
    {
        $this->uhrzeit = $uhrzeit;

        return $this;
    }

    /**
     * @return string
     */
    public function getTreffpunkt()
    {
        return $this->treffpunkt;
    }

    /**
     * @param string $treffpunkt
     */
    public function setTreffpunkt($treffpunkt)
    {
        $this->treffpunkt = $treffpunkt;

        return $this;
    }

    /**
     * @return string
     */
    public function getParkmoeglichkeit()
    {
        return $this->parkmoeglichkeit;
    }

    /**
     * @param string $parkmoeglichkeit
     */
    public function setParkmoeglichkeit($parkmoeglichkeit)
    {
        $this->parkmoeglichkeit = $parkmoeglichkeit;

        return $this;
    }

    /**
     * @return string
     */
    public function getWanderweg()
    {
        return $this->wanderweg;
    }

    /**
     * @param string $wanderweg
     */
    public function setWanderweg($wanderweg)
    {
        $this->wanderweg = $wanderweg;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getLaenge()
    {
        return $this->laenge;
    }

    /**
     * @param string $laenge
     */
    public function setLaenge($laenge)
    {
        $this->laenge = $laenge;

        return $this;
    }

    /**
     * @return string
     */
    public function getDauer()
    {
        return $this->dauer;
    }

    /**
     * @param string $dauer
     */
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodenbeschaffenheit()
    {
        return $this->bodenbeschaffenheit;
    }

    /**
     * @param string $bodenbeschaffenheit
     */
    public function setBodenbeschaffenheit($bodenbeschaffenheit)
    {
        $this->bodenbeschaffenheit = $bodenbeschaffenheit;

        return $this;
    }

    /**
     * @return string
     */
    public function getBesonderheiten()
    {
        return $this->besonderheiten;
    }

    /**
     * @param string $besonderheiten
     */
    public function setBesonderheiten($besonderheiten)
    {
        $this->besonderheiten = $besonderheiten;

        return $this;
    }

    /**
     * @return string
     */
    public function getSchwierigkeit()
    {
        return $this->schwierigkeit;
    }

    /**
     * @param string $schwierigkeit
     */
    public function setSchwierigkeit($schwierigkeit)
    {
        $this->schwierigkeit = $schwierigkeit;

        return $this;
    }

    /**
     * @return string
     */
    public function getTeilnahmegebuehr()
    {
        return $this->teilnahmegebuehr;
    }

    /**
     * @param string $teilnahmegebuehr
     */
    public function setTeilnahmegebuehr($teilnahmegebuehr)
    {
        $this->teilnahmegebuehr = $teilnahmegebuehr;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaxTeilnehmer()
    {
        return $this->maxTeilnehmer;
    }

    /**
     * @param string $maxTeilnehmer
     */
    public function setMaxTeilnehmer($maxTeilnehmer)
    {
        $this->maxTeilnehmer = $maxTeilnehmer;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }
    
    

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param array $images
     * @return $this
     */
    public function setImages($images)
    {
        if (count($images) > 0) {
            foreach ($images as $i) {
                $this->addImage($i);
            }
        }

        return $this;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Image $image
     *
     * @return Tour
     */
    public function addImage(\AppBundle\Entity\Image $image)
    {
        $image->setTour($this);
        $this->images->add($image);

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Image $image
     */
    public function removeImage(\AppBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection|Image[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return Image|null
     */
    public function getPreview()
    {
        /** @var Image[] $result */
        $result=null;
        foreach ($this->getImages() as $image) {
            if ($image->getParkmoeglichkeit()) {
                return $image;
            }
        }
        return $result;
//        /** @var Image $result */
//        $result=null;
//        foreach ($this->getImages() as $image) {
//                if (!$result || $image->getSortierung() < $result->getSortierung()) {
//                    $result = $image;
//                }
//        }
//        return $result;
    }
    /**
     * @return Image[]
     */
    public function getParkenImages()
    {
        /** @var Image[] $result */
        $result=[];
        foreach ($this->getImages() as $image) {
            if ($image->getParkmoeglichkeit()) {
                $result[] = $image;
            }
        }
        return $result;
    }
    /**
     * @return Image[]
     */
    public function getTreffpunkImages()
    {
        /** @var Image[] $result */
        $result=[];
        foreach ($this->getImages() as $image) {
            if ($image->getTreffpunkt()) {
                $result[] = $image;
            }
        }
        return $result;
    }
}
