<?
define("DB_TABLE_INIT_ERROR",1021);
define("DB_TABLE_VARIABLE_ERROR",1022);
define("DB_TABLE_INSERT_ERROR",1023);
define("DB_TABLE_UPDATE_ERROR",1024);
define("DB_TABLE_DELETE_ERROR",1025);

/**
 * DbTable-Class is responsible for mapping Object-Values to a Database
 *
 *
 * @package  PORA - PHP Object Relational Adapter
 * @version  1
 * @author   Thomas Roediger <t.roediger@vipermedia.de>
 * @since    PHP 4.0
 */
class DbTable extends PEAR
{
	/* Classname of the object that is mapped to database */
	var $className=null;
	/* name of the databasetable */
	var $tableName="";
	/* name of the databasetable without prefix, needed for joins*/
	var $npTableName="";
	/* array containing all information for mapping the object-Values to the table.
	* Each column/attribute is represented by an entry likes this:
	* 		"foo"=>array(
	*						"name"=>"foo",
	*						"type"=>"string",
	*						"altered"=>true,
	*						"value"=>"bar"
	*					  )
	*/
	var $tableValues=array();
	/* the fields of the table ready for beeing used in a select clause */
	var $selectFields="";
	/* array of join structures :
	* $join=array(
	"join_table"=>table-Object
	"referer"=>"pk"|"fk" // see DbObject
	)
	*/
	var $joins=array();
	// the alias of the table in joins
	var $joinName="";

	/* the name of the column that indicates, that the datarow is marked es deleted*/
	var $deletedFlag="";

	/**
     * Simple Constructor. Using the Constructor doesn�t make sense.
	 * DbTable-objects are created using the DbTable::factory()-method
     *
     * @access public
     */
	function DbTable(){
		$this->PEAR();
	}

	/**
     * creates a DbTable-object, anlaysing the table and the giving Object
     *
     * @param $i_tablename string name of the databasetable
     * @param $i_object string the object which will be connected to the table
	 *
     * @access public
     * @return object DbTable on success, a PEAR-Error on failure
	 */
	function factory( $i_tablename, &$i_object, $i_deletedFlag ){
		if (!$GLOBALS['PORA_CACHE'])
			$GLOBALS['PORA_CACHE']=array();
		if (!$GLOBALS['PORACACHE']['TABLE_PROTOTYPE'])
			$GLOBALS['PORACACHE']['TABLE_PROTOTYPE']=array();
		if(!isset($GLOBALS['PORACACHE']['TABLE_PROTOTYPE'][$i_tablename])){
			$table=new DbTable();

			$result=$table->init( $i_tablename, $i_object ,$i_deletedFlag);
			if ( PEAR::isError($result) ){
				return PEAR::raiseError($result);
			}
			$GLOBALS['PORACACHE']['TABLE_PROTOTYPE'][$i_tablename]=$table;
		}

		return clone $GLOBALS['PORACACHE']['TABLE_PROTOTYPE'][$i_tablename];
	}

	/**
     * initializes the DbTable-object
     *
     * @param $i_tablename string name of the databasetable
     * @param $i_object string the object which will be connected to the table
	 *
     * @access private
     * @return mixed true on success, a PEAR-Error on failure
	 */
	function init($i_tablename,&$i_object, $i_deletedFlag=""){
		global $CFG;
		$dbSession = &DbService::session($i_tablename);

		$this->tableName=$dbSession->table_prefix . $i_tablename;
		// tablename without Prefix, used for joins
		$this->npTableName=$i_tablename;
		$this->className= get_class($i_object);
		$this->deletedFlag = $i_deletedFlag;

		// initialize the array containing the column representations
		if($CFG->pora_dblayer=='adodb'){
			$tableInfo=$dbSession->tableInfo($this->tableName);
			if (PEAR::isError($tableInfo)){
				return PEAR::raiseError("DbTable: Error reading tableinformation. RootCause: " .$tableInfo->getMessage(),$tableInfo->getCode());
			}
		}else{
			@$qid=$dbSession->query('SHOW FIELDS FROM '.$i_tablename);
			if(mysql_errno())
				return PEAR::raiseError("DbTable: Error reading tableinformation. RootCause: " .mysql_error());
			$tableInfo=array();
			while($r=mysql_fetch_array($qid))
			$tableInfo[]=$r;

		}
		foreach( $tableInfo as $field ){
			if($CFG->pora_dblayer=='adodb'){
				$this->tableValues[$field->name] = array ("db_type"=>strtoupper($field->type), "type" => constant("DB_TYPE_" . strtoupper($field->type)), "in_object" => false, "altered" => false );
			}else{
				list($type)=preg_split("/(\(| )/",$field['Type']);
				$this->tableValues[$field['Field']] = array ("db_type"=>strtoupper($type), "type" => constant("DB_TYPE_" . strtoupper($type)), "in_object" => false, "altered" => false );
			}
		}

		$oid_name=OID_NAME;
		if ( !isset($i_object->$oid_name) ){
			if($CFG->pora_strict){
				return PEAR::raiseError(" DBTable: Missing ObjectID-attribute \"$oid_name\" in Class " . $this->className , DB_TABLE_INIT_ERROR);
			}
		}

		// linking the attribute-values
		$result = $this->getObjectValues($i_object);
		if (PEAR::isError($result)){
			return $result;
		}

		// checking if there are any columns that do not exist as an attribute
		$first=true;
		foreach ($this->tableValues as $name=>$value){
			if (!$value["in_object"]){
				if($CFG->pora_strict){
					return PEAR::raiseError(" DBTable: Missing attribute \"$name\" in Class " . $this->className .". Maybe it was not initialized." , DB_TABLE_INIT_ERROR);
				}else{
					unset($this->tableValues[$name]);
				}
			}else{
				$this->selectFields.= ($first ? "":",") . $this->tableName.".".$name;
				$first=false;
			}
		}
		return true;
	}

	function set($varname,$value,$markAsAltered=true){
		$this->tableValues[$varname]["value"]=$value;
		if ($markAsAltered)
			$this->tableValues[$varname]["altered"]=true;
	}

	/**
     * links the attributes of the object to the corresponding field representing a table-column
     *
     * @param $i_object string the object which will be connected to the table
	 *
     * @access private
     * @return mixed true on success, a PEAR-Error on failure
	 */
	function getObjectValues(&$i_object){
		global $CFG;
		$object_vars=get_object_vars($i_object);
		foreach($object_vars as $varname=>$value){
			// object-atttributes beginning with a "_" will be ignored
			if( $varname[0]!="_" ){
				// if the current attribute is an object it will be processed
				if (is_object($i_object->$varname)){
					$r = $this->getObjectValues($i_object->$varname);
					if (PEAR::isError($r)){
						return $r;
					}
				}else{
					// check if the type of the attribute fits to the one of the column
					if( array_key_exists($varname,$this->tableValues) ){
						$vartype=$this->tableValues[$varname]["type"];
						$func_name="is_$vartype";
						if(!$func_name($i_object->$varname)){
							if($CFG->pora_strict){
								return PEAR::raiseError(" DBTable: attribute $varname ($vartype) fron the class " . get_class($i_object) ." is of a wrong type (".gettype($i_object->$varname)."). ", DB_TABLE_VARIABLE_ERROR );
							}
						}
						$this->tableValues[$varname]["value"] = $i_object->$varname;
						$this->tableValues[$varname]["in_object"] = true;
					}else{
						if($CFG->pora_strict){
							return PEAR::raiseError(" DBTable: in table $this->tableName there is no column named $varname ." ,	DB_TABLE_VARIABLE_ERROR);
						}
					}
				}
			}
		}
		return true;
	}

	/**
     * insert the object into database table
     *
     * @access public
     * @return mixed Object-ID of the inserted obhect on success, a PEAR-Error on failure
	 */
	function insert(){
		$dbSession = &DbService::session($this->tableName);
		$nextOID=DbService::nextOID($this->tableName);
		if(PEAR::isError($nextOID)){
			return $nextOID;
		}
		$query="INSERT INTO " . $this->tableName . $this->getInsertString(OID_NAME,$nextOID);

		$result=$dbSession->query($query);
		if(PEAR::isError($result)){
			return  $this->raiseError(" DBTable: Error inserting Object.\RootCause:" . $result->getMessage() . ":" . $result->getUserInfo() ,
			DB_TABLE_INSERT_ERROR);;
		}
		return $nextOID;
	}

	/**
     * updates the object in database table
     *
     * @access public
     * @return mixed number of effected rows on success, a PEAR-Error on failure
	 */
	function update(){
		$dbSession = &DbService::session($this->tableName);
		$oidName=OID_NAME;
		$query="UPDATE " . $this->tableName .
		" SET " . $this->getUpdateString() .
		" WHERE " . $oidName . "=" . $this->tableValues[$oidName]["value"];

		$result=$dbSession->query($query,-1,-1,false);
		if(PEAR::isError($result)){
			return  $this->raiseError(" DBTable: Error updating Object.\RootCause:" . $result->getMessage(),
			DB_TABLE_UPDATE_ERROR);;
		}
		return  $dbSession->affectedRows();
	}

	/**
     * deletes the object from database table
     *
     * @access public
     * @return mixed number of effected rows on success, a PEAR-Error on failure
	 */
	function delete(){
		$dbSession = &DbService::session($this->tableName);
		$oidName=OID_NAME;
		$query="DELETE FROM " . $this->tableName .
		" WHERE " . $oidName . "=" . $this->tableValues[$oidName]["value"];

		$result=$dbSession->query($query,-1,-1,false);
		if(PEAR::isError($result)){
			return  $this->raiseError(" DBTable: Error deleting Object.\RootCause:" . $result->getMessage(),
			DB_TABLE_UPDATE_ERROR);;
		}
		return  $dbSession->affectedRows();
	}

	/**
     * performes a select statement
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "condition": the WHERE statement of the query
	 *					 "order_by" : which column is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 *
     * @access public
     * @return object DbCursor  on success, a PEAR-Error on failure
	 */
	function select( $i_options=array() ){

		$dbSession = &DbService::session($this->tableName);
		$query="SELECT DISTINCT $this->selectFields FROM $this->tableName" . $i_options['join'] ;
		$query.= isset($i_options["condition"]) ? " ".$i_options["condition"]:"";
		$query.= isset($i_options["order_by"]) ?" ORDER BY " . $i_options["order_by"]:"";
		//echo $query;
		if( isset($i_options["offset"]) && isset($i_options["count"]) ){
			$result=$dbSession->query( $query, $i_options["offset"], $i_options["count"] );
		}else{
			$result=$dbSession->query($query);
		}

		if(PEAR::isError($result)){
			return $result;
		}
		$cursor = &DbCursor::createCursor($result,$this->className);

		return $cursor;
	}

	function selectFieldFunc($i_options){
		$dbSession = &DbService::session($this->tableName);
		$selectFields="";
		$first=true;
		foreach ($i_options['fieldfuncs'] as $fieldfunc){
			$selectFields.= $first ?"":",";

			$selectFields.= "$fieldfunc[func]( $dbSession->table_prefix$fieldfunc[field] )";
			$first=false;
		}

		$query="SELECT $selectFields FROM $this->tableName" . $i_options['join'] ;
		$query.= isset($i_options["condition"]) ? " ".$i_options["condition"]:"";
		//echo $query;
		$GLOBALS['ADODB_FETCH_MODE'] = ADODB_FETCH_BOTH;
		if ($i_options['all_rows']==true){
			$result = $dbSession->getAll($query);
		}else{
			$result = $dbSession->getRow($query);
		}
		$GLOBALS['ADODB_FETCH_MODE'] = ADODB_FETCH_ASSOC;
		return $result;
	}

	/**
     * performes a select statement. Analyses which attributes of the object
	 * are set an uses them as conditions in the query.
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "condition": the WHERE statement of the query
	 *					 "order_by" : which column is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 * @param $i_mode String contains the boolean operation the several conditions are linked with
	 *
     * @access public
     * @return object DbCursor  on success, a PEAR-Error on failure
	 */
	function queryByExample( $i_options=array(), $i_mode="AND" ){

		$options=$this->getQueryByExampleConditions($i_options, $i_mode);
		$options["condition"]=strlen($options["condition"]) ? "WHERE ".$options["condition"] : "";
		return $this->select($options);
	}

	function fieldFuncByExample( $i_options=array(), $i_mode="AND" ){

		$options=$this->getQueryByExampleConditions($i_options, $i_mode);
		$options["condition"]=strlen($options["condition"]) ? "WHERE ".$options["condition"] : "";
		return $this->selectFieldFunc($options);
	}

	/**
     * Analyses which attributes of the object are set an uses them as conditions in the query.
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "condition": the WHERE statement of the query
	 *					 "order_by" : which column is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 * @param $i_mode String contains the boolean operation the several conditions are linked with
	 *
     * @access private
     * @return array Options
	 */
	function getQueryByExampleConditions($i_options,$i_mode){

		//$i_options["condition"]="";
		if( $i_mode!="AND" && $i_mode!="OR" ){
			$i_mode="AND";
		}

		foreach($this->joins as $key=>$join){
			$join['table']->joinName=$join['table']->tableName."join$key";
			$i_options=$this->getJoinOptions($i_options,$join);
		}

		$appendDelCondition="";

		$tablename=strlen($this->joinName)?$this->joinName:$this->tableName;

		foreach($this->tableValues as $varname=>$value){
			if(($value["type"]=="integer" || $value["type"]=="double") && $varname != $this->deletedFlag ){
				if($value["value"]||$value["altered"]){
					$i_options["condition"] .= strlen($i_options["condition"])?" $i_mode ":"";
					$i_options["condition"].=$tablename.".$varname=$value[value]";
				}
			}else if($value["type"]=="string" && $varname != $this->deletedFlag ){
				if(strlen($value["value"])||$value["altered"]){
					$i_options["condition"].=strlen($i_options["condition"])?" $i_mode ":"";
					if($i_options["unsharp"]==true){
						$i_options["condition"].=$tablename.".$varname LIKE \"%".addslashes($value[value])."%\"";
					}else{
						$i_options["condition"].=$tablename.".$varname LIKE '".addslashes($value[value])."'";
					}
				}
				// if the deleted-flag is set, the condition is assigned
			}else if($varname == $this->deletedFlag){
				if (!empty($value['value'])){
					$appendDelCondition=$tablename.".$varname='$value[value]' ";
				}
			}
		}
		// if the delete Condition is not empty its appendes via AND
		if(strlen($appendDelCondition))
		$i_options["condition"]=strlen($i_options["condition"])?"(" . $i_options["condition"] . ") AND $appendDelCondition": $appendDelCondition;

		$i_options["condition"]=strlen($i_options["condition"])? "(" . $i_options["condition"] . ")" :$i_options["condition"];
		return $i_options;
	}


	/**
     *
     *
	 * @param $i_options Array The Select-options array @see DbTable::select()
	 * @param $i_join Array an array configuring the join operation
	 *            	"referer" => Two possible values: "fk" -> The table's refering column is an foreing key to an other table
	 *	             								   "pk" -> The table's primary key is referenced by another table
	 *	          	"table"   => An DbTable-object which represents the table to join to
	 *
     * @access private
     * @return array Options
	 */
	function getJoinOptions($i_options,$i_join){
		$tablename=strlen($this->joinName)?$this->joinName:$this->tableName;
		if (strlen($i_join['key_column'])){
			$i_options['join'].=" LEFT JOIN ". $i_join['table']->tableName . " AS ".$i_join[table]->joinName." USING ($i_join[key_column])";
		}elseif ($i_join['referer']=="fk"){
			$i_options['join'].=" LEFT JOIN ". $i_join['table']->tableName . " AS ".$i_join[table]->joinName." ON " . $tablename . "." . $i_join['table']->npTableName . "OID=". $i_join[table]->joinName.".OID ";
		}elseif ($i_join['referer']=="pk"){
			$i_options['join'].=" LEFT JOIN ". $i_join['table']->tableName . " AS ".$i_join[table]->joinName." ON " . $tablename . ".OID=" . $i_join[table]->joinName . "." . $this->npTableName . "OID ";
		}else{
			return $i_options;
		}

		// if there are several Contitions set in the Table-Object, the have got to be resolved
		$i_options=$i_join['table']->getQueryByExampleConditions($i_options,"AND");

		return $i_options;
	}

	/**
	 * Creates as String containing the values that will be inserted in Databases.
	 *
     * @param $oid_name String The name of the OID Column
     * @param $OID The Objects OID
     * @access private
     * @return String
	 */
	function getInsertString($oid_name="",$OID=""){
		$dbSession = &DbService::session($this->tableName);
		$query="";
		$cols=array();
		$qValues=array();
		foreach($this->tableValues as $varname=>$value){
			if ( $varname != OID_NAME && $this->tableValues[$varname]["db_type"]!="TIMESTAMP"){
				$cols[]=$varname;
				$qValues[]=$this->quoteString($varname,$value);
			}
		}
		if(strlen($oid_name) && $OID){
			$cols[]=OID_NAME;
			$qValues[]=$OID;
		}

		$query="(". implode(',',$cols) . ") VALUES (" . implode(',',$qValues) . ") ";

		return $query;
	}

	/**
	 * Creates as String containing the values that will be updated in Databases.
	 *
     * @access private
     * @return String
	 */
	function getUpdateString(){
		$first=1;
		$query="";
		foreach($this->tableValues as $varname=>$value){
			if ( $varname != OID_NAME && $this->tableValues[$varname]["db_type"]!="TIMESTAMP"){
				$query.=$first?"":",";
				$qValue=$this->quoteString($varname,$value);
				$query.="$varname=" . $qValue;
				$first=0;
			}
		}
		return $query;
	}


	/**
	 * quote the String dedending on its type
	 *
	 * @param $varname String the Column/Objektvar
	 * @param $value Array a Tablevalue @see DbTable
     * @access private
     * @return String
	 */
	function quoteString($varname,$value){
		global $CFG;
		$dbSession = &DbService::session($this->tableName);
		switch($this->tableValues[$varname]["type"]){
			case "integer":
			case "double":
				$result=$value["value"];
				break;
			default:
				if($CFG->pora_dblayer=='adodb'){
					$result=$dbSession->dbh->quote($value["value"]);
				}else {
					$result="'".addslashes($value["value"])."'";
				}
				break;

		}
		return $result;
	}


}
?>
