<?
/**
 *
 * @package  PORA - PHP Object Relational Adapter
 * @version  1
 * @author   Thomas Roediger <t.roediger@vipermedia.de>
 * @since    PHP 4.0
 */

define("OID_NAME","OID");
define("OID_SUFFIX","OID");

/**
 * @package PORA
 */
class DbApplication{
	var $dsn="";
	
	function openDb(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	}
	
	function dsn($i_dsn=""){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	}
	
	function createTables(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	}
	
	function deleteTables(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	}
	
	function createOIDTable(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	}
	
	function nextOID(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	} 
	
	function maxOID(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	} 
	
	function incrementOID(){
		return PEAR::raiseError("This method has got to be implemented by Childclass");
	} 
	

}
?>