<?

define("PORA_DIR","../../../pora.ado/");
require("../../../../config.php");
$CFG->pora_dblayer='adodb';
$form=$_REQUEST;
$output="";
$dbSession=null;


ini_set("magic_quotes_gpc","1");
ini_set("session.name","SID");
ini_set("register_globals","off");
umask("0000");

require( PORA_DIR . "DbService.class.php" );
// wir sp�ter benutzt um die Konfigurationsvariablen aufzunehmen.
if (isset($form['action_submit'])){

	$dbSession=&initDb();
	$output=generateDbClasses($form);
}

function &initDb(){
	global $db;
	$db=DbService::application("DbVmFrameworkApplication");
	if(PEAR::isError($db))
	{
		die($db->getMessage .  "<br>" . $db->getUserMessage()) ;
	}
//	return DbService::session();
	return true;
}

function generateDbClasses($i_form){
	global $CFG;

	$usedTables=array_merge(array('lokaleTabellen'=>'default'),$CFG->pora_tables);
	$output="";
	foreach($usedTables as $tblName=>$connName){
		$dbSession=DbService::session($tblName);

		if($tblName=='lokaleTabellen'){
			$tables=$dbSession->tableList();
		}else{
			$tables=array($tblName);
		}

		if(is_writeable($CFG->generator_output_dir)){
			foreach ($tables as $table){
				ob_start();
				dumpHeader($table);
				dumpConstructor($table);
				$tableInfo=$dbSession->tableInfo($table);
				foreach($tableInfo as $field){
					dumpAccessor($field);
				}
				dumpFooter();
				$dbClassCode=ob_get_contents();
				ob_end_clean();
				ob_start();
				dumpLgClass($table);
				$lgClassCode=ob_get_contents();
				ob_end_clean();
				// schreiben der db Klasse
				$filename=$CFG->generator_output_dir."/"."Db".ucfirst($table).".class.php";
				if (strtolower($table) != "maxoid"){
					$fp=fopen($filename,"w");
					$r=fwrite($fp,$dbClassCode)!=false;
					if ($r!=false){
						$output.="$filename wurde geschrieben<br>";
					}
					fclose($fp);
				}
				if ($i_form['gen_lg'] && strtolower($table) != "maxoid"){
					// schreiben der LG Klasse
					$filename=$CFG->generator_output_dir."/"."Lg".ucfirst($table).".class.php";
					$fp=fopen($filename,"w");
					$r=fwrite($fp,$lgClassCode)!=false;
					if ($r!=false){
						$output.="$filename wurde geschrieben<br>";
					}
					fclose($fp);
				}
			}
		}else{
			$output.= $CFG->output_dir . " ist nicht schreibbar.";
		}
	}

	return $output;
}

function dumpHeader($table){
	echo '<?

';
	echo 'class Db' .ucfirst($table) . ' extends DbObject{';
}

function dumpFooter(){
	echo '
}
?>';
}

function dumpConstructor($table){
	echo '

	function Db'.ucfirst($table).'(){
	 	$this->DbObject("'.$table.'");
	 	$this->setOID(0);
	}
';
}

function dumpAccessor($field){
	$type=constant("DB_TYPE_" . strtoupper($field->type));
	switch ($type){
			case "integer":
				if ($field->name=="OID"){
				}elseif( substr($field->name,-3) == "OID" ){
					$classname=str_replace (  "OID","",$field->name);
					dumpObjectAccessor($field->name,$classname);
				}else{
					dumpIntegerAccessor($field->name);
				}
				break;
			case "string":
					dumpStringAccessor($field->name);
				break;
			case "double":
					dumpDoubleAccessor($field->name);
				break;
	}
}

function dumpObjectAccessor($fieldname,$classname){
	echo'
	var $'.$fieldname.'=0;

	function set'.ucfirst($classname).'( $i_value ){
		$this->set(\''.$fieldname.'\',$i_value->getOID() );
	}

	function get'.ucfirst($classname).'(){
		$query=new Lg'.ucfirst($classname).';
		return $query->selectByOID($this->'.$fieldname.');
	}
';
}

function dumpIntegerAccessor($fieldname){
	echo'
	var $'.$fieldname.'=0;

	function set'.ucfirst($fieldname).'( $i_value ){
		$this->set(\''.$fieldname.'\',(int)$i_value );
	}

	function get'.ucfirst($fieldname).'(){
		return $this->'.$fieldname.';
	}
';
}

function dumpStringAccessor($fieldname){
	echo'
	var $'.$fieldname.'="";

	function set'.ucfirst($fieldname).'( $i_value ){
		$this->set(\''.$fieldname.'\',(string)$i_value );
	}

	function get'.ucfirst($fieldname).'(){
		return $this->'.$fieldname.';
	}
';
}

function dumpDoubleAccessor($fieldname){
	echo'
	var $'.$fieldname.'=0.0;

	function set'.ucfirst($fieldname).'( $i_value ){
		$this->set(\''.$fieldname.'\',(double)$i_value );
	}

	function get'.ucfirst($fieldname).'(){
		return $this->'.$fieldname.';
	}
';
}

function dumpLgClass($table){
	echo '<?

require_once("db/Db' .ucfirst($table) . '.class.php");

class Lg' .ucfirst($table) . ' extends Db' .ucfirst($table) . '{

	function Lg' .ucfirst($table) . '(){
		$this->Db' .ucfirst($table) . '();
	}
}

?>
';
}


if (isset($form['action_submit'])){
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title></title>
</head>
<body>
	<?=$output?>
</body>
</html>
<?
}else{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title></title>
</head>
<body>
<table>
<form action="<?=$_SERVER['PHP_SELF']?>">
<tr>
	<td colspan="2"><input type="checkbox" name="gen_lg" value="1">&nbsp;LgKlassen generieren</td>
</tr>
<tr>
	<td colspan="2"><input type="submit" name="action_submit" value="DbKlassen generieren!"></td>
</tr>
</form>
</table>
</body>
</html>
<?
}
?>
