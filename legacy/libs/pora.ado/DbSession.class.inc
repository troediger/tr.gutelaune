<?

define ("DB_SESSION_CONNECT_ERROR",1000);
define ("DB_SESSION_TRANSACTION_ERROR",1001);
define ("DB_SESSION_QUERY_ERROR",1002);

/**
 * DbSession is responsible managing the current Databese session.
 *
 * It provides all methods that are needes to acess the database
 * such as connect- or query- or fetch- Methods
 *
 * @package  PORA - PHP Object Relational Adapter
 * @version  1
 * @author   Thomas Roediger <t.roediger@vipermedia.de>
 * @since    PHP 4.0
 */
class DbSession extends PEAR
{

	/**
	 * contains the current DB-object
	 */
	var $dbh;
	/**
	 * contains the current connection-string
	 */
	var $dsn;

	var $table_prefix="";

	function DbSession($i_dsn,$i_table_prefix=""){
		global $CFG;
#		$CFG=Controller::config();
		$this->PEAR();
		$this->dsn=DB::parseDSN($i_dsn);
		$this->table_prefix=$i_table_prefix;
		include_once( PORA_DIR . "typemap_$CFG->type_map.inc" );
	}

	function _DbSession(){
		$this->setErrorHandling(PEAR_ERROR_TRIGGER,E_USER_WARNING);
		//$r=$this->endTransaction(false);
		//$this->close();
	}

	function connect(){
		global $CFG;
		if($CFG->pora_dblayer=='adodb'){
			$this->dbh=&DB::connect($this->dsn);
			$this->dbh->debug =$CFG->ado_debug;
			if (PEAR::isError($this->dbh))
				return $this->dbh;
		}else{
			$this->dbh = mysql_connect($this->dsn['hostspec'],$this->dsn['username'],$this->dsn['password'],true);
			@mysql_select_db($this->dsn['database'],$this->dbh);
			//error_log('connect '.date('Ymd H:i:s')."\n",3,'/tmp/moderat_connects.txt');
			return $this->dbh;
		}

		return true;
	}

	function beginTransaction(){
		$this->dbh->BeginTrans();
		return true;
	}

	function endTransaction($i_ok){
		if($i_ok){
			return $this->dbh->CommitTrans();
		}else{
			return $this->dbh->RollbackTrans();
		}
	}

	function &getLink(){
		return $this->dbh;
	}

	function close(){
		global $CFG;
		if($CFG->pora_dblayer=='adodb'){
			return  $this->dbh->Close();
		}else{
			return  mysql_close($this->dbh);
		}
	}

	function &query($i_query,$i_offset=-1,$i_count=-1, $i_use_cache=true){
		global $CFG;
		if($CFG->pora_dblayer=='adodb'){
			if($i_use_cache==true && PORA_USE_CACHE){
				if( $i_count>0 )
					return $this->dbh->CacheSelectLimit($i_query, $i_count, $i_offset);
				return $this->dbh->CacheExecute($i_query);
			}else{
				if( $i_count>0 )
					return $this->dbh->SelectLimit($i_query, $i_count, $i_offset);
				return $this->dbh->Execute($i_query);
			}
		}else{
			if( $i_count>0 ){
				$limit=" LIMIT $i_offset,$i_count ";
			}
			$result = mysql_query($i_query.$limit,$this->dbh);
			if (mysql_errno()){
				return PEAR::raiseError("Error executing Query: ".mysql_error() . "::Query was: ". $i_query.$limit,DB_SESSION_QUERY_ERROR);
			}
			return $result;
		}
	}

//	function &numRows(){
//		return mysql_num_rows($this);
//	}

	function &getOne($i_query){
		global $CFG;

		if($CFG->pora_dblayer=='adodb'){
			return $this->dbh->getOne($i_query);
		}else{
			$dbSession=DbService::session();
			$tmp=mysql_fetch_row($dbSession->query($i_query));
			return $tmp[0];
		}

	}

	function &getRow($i_query){
		global $CFG;
		if($CFG->pora_dblayer=='adodb'){
			return $this->dbh->getRow($i_query);
		}else{
			$dbSession=DbService::session();
			$tmp=mysql_fetch_array($dbSession->query($i_query));
			return $tmp;
		}
	}

	function &getAll($i_query){
		global $CFG;
		if($CFG->pora_dblayer=='adodb'){
			return $this->dbh->getAll($i_query);
		}else{
			$rs = $this->query($i_query);
			$result = array();
			while($row = mysql_fetch_array($rs)){
				$result[] = $row;
			}
			
			return $result;
		}
	}

	function affectedRows(){
		global $CFG;
		if($CFG->pora_dblayer=='adodb'){

			return $this->dbh->Affected_Rows();
		}else{
			return mysql_affected_rows($this->dbh);
		}
	}

	function tableInfo($i_tableName=""){
		return $this->dbh->MetaColumns($i_tableName);
	}

	function tableList($i_tableName=""){
		return $this->dbh->MetaTables($i_tableName);
	}

}
?>
