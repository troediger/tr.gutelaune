<?

define("DB_CURSOR_ERROR",1050);
define("DB_CURSOR_POINTER_ERROR",1050);

/**
 * DbCursor manages a DbResult-object provides methodes to get an object
 * of the correct class from the database
 *
 *
 * @package  PORA - PHP Object Relational Adapter
 * @version  1
 * @author   Thomas Roediger <t.roediger@vipermedia.de>
 * @since    PHP 4.0
 */
class DbCursor  extends PEAR
{
        /**
         * DbResult-object representing the result of a query
         */
        var $dbResult;
        /**
         * classname of the class that shall be returned
         */
        var $className;
        /**
         * indicates the current row of the resultset
         */
        var $rowPointer=null;
        /**
         * indicates the first row of the resultset
         */
        var $first=0;
        /**
         * indicates the last row of the resultset
         */
        var $last=0;

        function DbCursor(){
                $this->PEAR();
        }

        function &createCursor(&$i_dbResult,$i_className){
				global $CFG;
                //$dbSession=&DbService::session();
                $cursor=new DbCursor();
				if($CFG->pora_dblayer=='adodb'){
	                if( ! preg_match("/adorecordset_.*/i",get_class($i_dbResult) ) )
	                {
	                        return $this->raiseError("DBCursor: no result-object." , DB_CURSOR_ERROR);
	                }
	                $cursor->last=$i_dbResult->RecordCount()-1;
				}else{
					$test=mysql_num_rows($i_dbResult);
	                $cursor->last=mysql_num_rows($i_dbResult)-1;
				}
                $cursor->dbResult=&$i_dbResult;
                $cursor->className=$i_className;
                $cursor->rowPointer=-1;
                $cursor->first=$cursor->last==-1?-1:0;

                return $cursor;
        }

        function next()
        {
                $this->rowPointer++;
                if($this->rowPointer > $this->last)
                {
                        $this->rowPointer--;
                        return null;
                }
                return $this->fetch();
        }

        function prev()
        {
                $this->rowPointer--;
                if($this->rowPointer < $this->first )
                {
                        $this->rowPointer++;
                        return null;
                }
                return $this->fetch();
        }

        function first()
        {
                $this->rowPointer=$this->first;
                if($this->rowPointer==-1)
                {
                        return null;
                }else
                {
                        return $this->fetch();
                }

        }

        function last()
        {
                $this->rowPointer=$this->last;
                if($this->rowPointer==-1)
                {
                        return null;
                }else
                {
                        return $this->fetch();
                }
        }

        function seekHead()
        {
                $this->rowPointer=$this->first-1;
        }

        function seekTail()
        {
                $this->rowPointer=$this->last+1;
        }

        function &fetch(){
				global $CFG;

				if($CFG->pora_dblayer=='adodb'){
					$this->dbResult->Move($this->rowPointer);
	                $resultArray=$this->dbResult->FetchRow();
	                if( PEAR::isError($resultArray)){
	                        return $this->raiseError(" DBCursor->fetch(): Die Datenreihe ($this->rowPointer) fuer $this->className konnte nicht geholt werden.\mysql:" . mysql_error(),
	                                                                                         DB_CURSOR_ERROR);
	                }
				}else{
					if(!mysql_data_seek ($this->dbResult, $this->rowPointer)){
						return $this->raiseError(" DBCursor->fetch(): Die Datenreihe ($this->rowPointer) fuer $this->className konnte nicht geholt werden.\mysql:" . mysql_error(),
																							 DB_CURSOR_ERROR );
					}

					$resultArray=mysql_fetch_array($this->dbResult);
					if(!is_array($resultArray) && !sizeof($resultArray)){
						return $this->raiseError(" DBCursor->fetch(): Die Datenreihe ($this->rowPointer) fuer $this->className konnte nicht geholt werden.\mysql:" . mysql_error(),
																							 DB_CURSOR_ERROR );
					}
				}

                $className=$this->className;
                $resultObject=new $className();
                $this->applyResultset($resultObject,$resultArray);
                return $resultObject;

        }

        function applyResultset(&$i_object,&$i_result)
        {
                $object_vars=get_object_vars($i_object);
                foreach($object_vars as $varname=>$value)
                {
                        if( $varname[0]!="_" && is_array($i_object->_table->tableValues[$varname]))
                        {
                            $i_object->$varname = $i_result[$varname];
                            settype($i_object->$varname,$i_object->_table->tableValues[$varname]["type"]);
                            $i_object->_table->tableValues[$varname]["value"]= $i_result[$varname];
                        }
                }
        }

        function numRows()
        {
              return $this->last+1;
        }
}
?>
