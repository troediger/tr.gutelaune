<?

define("DB_OBJECT_NOT_FOUND_ERROR",1900);
define("DB_OBJECT_QUERY_ERROR",1901);

/**
 * DbObject is baseclass the for all objects that will be stred in the database
 *
 * Wraps DbTable-functionalty
 *
 * @package  PORA - PHP Object Relational Adapter
 * @version  1
 * @author   Thomas Roediger <t.roediger@vipermedia.de>
 * @since    PHP 4.0
 */
class DbObject extends PEAR
{
	var $OID=0;
	var $_table=null;
	var $_error=null;
	var $_joins=array();
	// the folowing 2 properties are obsolete
	var $_scheduled_relations_overwrite=array();
	var $_scheduled_relations_insert=array();

	/**
	 * Constructor.
	 * @param $i_tableName String case-sensitive name of the table belonging to the Object
	 */
	function DbObject($i_tableName){
		$this->PEAR();
		$this->_table = DbTable::factory( $i_tableName, $this , DELETED_FLAG);
		if (PEAR::isError($this->_table )){
			#return $this->_table ;
			trigger_error("Error initializing $i_tableName <br>\n".$this->_table->getMessage(), E_USER_ERROR);
			$this->_error=$this->_table;
		}
	}

	/**
	 * Sets the OID of die Object.
	 * @param $i_oid integer
	 */
	function setOID($i_oid){
		$oidName=OID_NAME;
		$this->set($oidName,(int)$i_oid,false);
	}

	/**
	 * Bets the OID of die Object
	 * @return  integer
	 */
	function getOID(){
		$oidName=OID_NAME;
		#$this->$oidName;
		return $this->$oidName;
	}

	/**
	 * This setMethod has got to be called by the Objects set Operations. The first Parameter
	 * is the Name of the Objects Property and the second one is the value
	 * @param $varname String
	 * @param $value mixed
	 */
	function set($varname,$value,$markAsAltered=true){
		$this->$varname=$value;
		$this->_table->set($varname,$value,$markAsAltered);
	}

	/**
	 * Adds an Object as a Join for later querying. The Object given as parameter may
	 * have set value which will be used by an queryByExample
	 * Example:
	 * <pre>
	 * // finds all employees from Berlin-Departments
	 * $query=new LgEmployee();
	 * $join=new LgDepartment();
	 * $join->setCity("Berlin");
	 * $query->addJoin($join,"fk");
	 * $cursor=$query->queryByExample();
	 * </pre>
	 * referer may be
	 * <ul>
	 *	<li>"fk" if the joinedObject is referenced by an foreign Key or
	 *  <li>"pk" if the joinedObject references the current Objekt by a foreign key
	 * </ul>
	 */
	function addJoin(&$i_joinedObject, $i_referer,$i_key_column=""){
		$join=array(
				"table"=>&$i_joinedObject->_table,
				"referer"=>$i_referer

			 );
		if (strlen($i_key_column)){
			$join['key_column']=$i_key_column;
		}
		$this->_table->joins[]=$join;

	}

    /**
     * performs a select statement
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "condition": the WHERE statement of the query
	 *					 "order_by" : which column(s) is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 *
     * @return object DbCursor  on success, a PEAR-Error on failure
	 */
	function select($i_options=array()){
		return $this->_table->select($i_options);
	}

    /**
     * selects all Data for a table
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "order_by" : which column(s) is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 *
     * @return object DbCursor  on success, a PEAR-Error on failure
	 */
	function selectAll($i_options=array()){
		$i_options["condition"]="";
		return $this->_table->select($i_options);
	}

    /**
     * counts all Data for a table
     *
     * @return integer
	 */
	function countAll(){
		$cursor=$this->selectAll();
		return $cursor->numRows();
	}

	function loadByOid($i_class,$i_oid){
		$object=new $i_class();
		return $object->selectByOid($i_oid);
	}

    /**
     * selects an Object by its OID
     *
     * @param $i_oid OID of the Object
	 *
     * @return object DbObject  on success, a PEAR-Error on failure
	 */
	function selectByOid($i_oid){
		$cursor=$this->_table->select( array("condition"=>"WHERE OID=$i_oid") );
		if (PEAR::isError($cursor) || !$cursor){
			return null;
		}
		$r=$cursor->next();
		if (PEAR::isError($r) || !$r){
			return $this->raiseError(" DBObject->select_by_oid(): Das Objekt mit der OID $i_oid existiert nicht",
											 DB_OBJECT_NOT_FOUND_ERROR,
											 E_USER_ERROR,
											 NULL,
											 "Objekt nicht gefunden",
											 NULL
											 );
		}
		return $r;
	}

    /**
     * uses the Object as a Template for a query. Any property which is set will be used
     * as a SQL-condition
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "condition": the WHERE statement of the query
	 *					 "order_by" : which column(s) is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 * 					 "unsharp"  : true or false. false by default. Any String will be wraped in "%$string%" if true
	 * @param $i_mode String Maybe "OR" or "AND", used for joining conditions
	 *
     * @return object DbCursor  on success, a PEAR-Error on failure
	 */
	function queryByExample($i_options=array(),$i_mode="AND"){
		return $this->_table->queryByExample($i_options,$i_mode,$this->_joins);
	}

    /**
     * uses the Object as a Template for a query. Any property which is set will be used
     * as a SQL-condition
     *
     * @param $i_options array an associative array conaining query-options:
	 *					 "condition": the WHERE statement of the query
	 *					 "order_by" : which column(s) is the result ordered by
	 *					 "offset"   : the offset-part of a LIMIT - clause
	 *					 "count"    : the count-part of a LIMIT - clause
	 *					 "fieldfuncs"    : array of arrays with the fieldfunctionname as key and the fieldname as value
	 * 					 "unsharp"  : true or false. false by default. Any String will be wraped in "%$string%" if true
	 * @param $i_mode String Maybe "OR" or "AND", used for joining conditions
	 *
     * @return Array The Array depends on the number of fieldfunction given in i_options. each value of the array represents a result of one fieldfunction.
	 */
	function fieldFuncByExample($i_options=array(),$i_mode="AND"){
		return $this->_table->fieldFuncByExample($i_options,$i_mode,$this->_joins);
	}


    /**
     * inserts the object into database
     *
     * @return boolean true  on success, a PEAR-Error on failure
	 */
	function insert(){
		$oid= $this->_table->insert();
		if(PEAR::isError($oid))
		{
			return $oid;
		}
		$this->setOID( $oid);
		return true;
	}

    /**
     * updates the object in database
     *
     * @return boolean true  on success, a PEAR-Error on failure
	 */
	function update(){
		return $this->_table->update();
	}

    /**
     * saves the Object in Database. The method determines by the existience of the OID
     * of the objekt if it should be inserted or updated. If there is a function called
     * "checkValidity" in the Object, the function will be called before saving. checkValidity
     * has got to return an array containing the Errors or an empty Array.
     *
     * @return boolean true  on success, a PEAR-Error on failure
	 */
	function save(){
		$oidName=OID_NAME;
		$ok=false;

		$checkvalidity=array_search("checkvalidity",get_class_methods ( get_class($this) ) );
		if($checkvalidity){
	        $errors=$this->checkValidity();
	        if(sizeof($errors))
	        	return $errors;
		}

		$preSave=array_search("preSave",get_class_methods ( get_class($this) ) );
		if($preSave){
	        $errors=$this->$preSave();
	        if(sizeof($errors))
	        	return $errors;
		}

		if ($this->$oidName){
			$result=$this->update();
		}else{
			$result=$this->insert();
		}
		if(!PEAR::isError($result)){
			$ok=true;
		}

		return $result;
	}

    /**
     * deletes the object from database
     *
     * @return boolean true  on success, a PEAR-Error on failure
	 */
	function delete(){
		return $this->_table->delete();
	}

    /**
     * returnes all Properties from the Object
     *
     * @return Array
	 */
	function getAllProperties(){
		$result=array();
		$properties=get_object_vars($this);
		foreach ($properties as $key=>$value) {
			if(preg_match("/^([a-zA-Z])(.*)/",$key,$matches)){
				$result[$matches[1].$matches[2]]=$value;
			}
		}
		return $result;
	}

	/**
     * returns all Properties from the Object
	 *
	 * @param Array $properties
	 * @param String $classname
   *
   * @return Object
	 */
	function &setProperties($properties,$classname=""){
		if (!$this){
			$object=new $classname();
		}else{
			$object=&$this;
		}
		foreach ($properties as $key=>$value) {
			$funcname="set".$key;
			if($key=='OID' && $value!=""){
				$object=$object->selectByOid($value);
				if(PEAR::isError($object)){
					return $object;
				}
			}
			if(method_exists ($object,$funcname) && $key!="OID"){
				$object->$funcname($value);
			}else{
				if($key!="OID"){
					return PEAR::raiseError("Methode $funcname (Spalte \"$key\") ist ung�ltig");
				}
			}
		}
		return $object;
	}



}
?>
