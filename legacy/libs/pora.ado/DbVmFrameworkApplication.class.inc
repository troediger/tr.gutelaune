<?

//define("OID_SUFFIX","OID");
//define("OID_NAME","OID");
define("DELETED_FLAG",$GLOBALS['CFG']->pora_deleted_flag);
define("PORA_USE_CACHE",$GLOBALS['CFG']->pora_use_cache);

class DbVmFrameworkApplication extends DbApplication{

	function openDb($i_connectionId='default'){
		global $CFG;
		$dsn=DbVmFrameworkApplication::dsn($i_connectionId);
		if (!strlen($dsn)){
			$r=PEAR::raiseError("Diese Datenbankverbindung ist nicht konfiguriert",null,null,null,"Diese Datenbankverbindung ist nicht konfiguriert");
			return $r;
		}
		$dbSession=new DbSession($dsn,$CFG->table_prefix);
		$r=$dbSession->connect();
		if (PEAR::isError($r))
			return $r;
		return $dbSession;
	}

	function dsn($i_connectionId){
		global $CFG;
		static $_dsn=array();
		if (!$_dsn[$i_connectionId]){
			$_dsn[$i_connectionId]=$CFG->pora_connections[$i_connectionId]['dsn'];
		}
		return $_dsn[$i_connectionId];
	}

	function createTables(){

		return true;
	}

	function deleteTables(){
		return true;
	}

	function createOIDTable($i_deleteTable=false){
		$dbSession=&DbService::session();

		$maxOIDTable=$dbSession->table_prefix . "maxOID";

		if($i_deleteTable){
			$result=$dbSession->query("DROP TABLE $maxOIDTable");
			if(PEAR::isError($result)){
				return $result;
			}
		}

		$result=$dbSession->query("CREATE TABLE $maxOIDTable(maxOID int)");
		if(PEAR::isError($result)){
			return $result;
		}

		$result=$dbSession->query("INSERT INTO  $maxOIDTable SET maxOID=0");
		if(PEAR::isError($result)){
			return $result;
		}

	}

	function nextOID($i_tablename){
		$dbSession=&DbService::session($i_tablename);

		$maxOIDTable=$dbSession->table_prefix . "maxOID";


		/*$result=$dbSession->query("LOCK TABLES $maxOIDTable WRITE");
		if(PEAR::isError($result)){
			return $result;
		}*/
		$maxOID=&$this->maxOID($i_tablename);
		if(PEAR::isError($maxOID)){
			return $result;
		}
		$nextOID=$this->incrementOID($maxOID);

		$result=$dbSession->query("UPDATE $maxOIDTable SET maxOID=$nextOID");
		if(PEAR::isError($result)){
			return $result;
		}
		/*$result=$dbSession->query("UNLOCK TABLES");
		if(PEAR::isError($result)){
			return $result;
		}*/
		return $nextOID;
	}

	function &maxOID($i_tablename){
		$dbSession=&DbService::session($i_tablename);
		$maxOIDTable=$dbSession->table_prefix . "maxOID";
		return $dbSession->getOne("SELECT maxOID FROM $maxOIDTable");
	}

	function incrementOID($i_currentMax){
		return ++$i_currentMax;
	}
}
?>
