<?
if(!defined('PORA_DIR')){
	trigger_error( "Constant PORA_DIR is not defined", E_USER_ERROR);
}
require_once( $CFG->ado_dir ."adodb-pear.inc.php");
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$ADODB_CACHE_DIR = $CFG->ado_cache_dir;
define('ADODB_ASSOC_CASE', $CFG->ado_case);

if (version_compare(phpversion(), "5.0")==-1){
	$phpversion=4;
}else{
	$phpversion=5;
}

require_once( PORA_DIR . "DbApplication.class.inc" );
require_once( PORA_DIR . "DbSession.class.inc" );
require_once( PORA_DIR . "DbTable.php$phpversion.class.inc" );
require_once( PORA_DIR . "DbCursor.class.inc" );
require_once( PORA_DIR . "DbObject.class.inc" );

/**
 * DbService contains static operations for database access.
 *
 *
 * @package  PORA - PHP Object Relational Adapter
 * @version  1
 * @author   Thomas Roediger <t.roediger@vipermedia.de>
 * @since    PHP 4.0
 */
class DbService{
	function &application($i_db_application=""){
		static $_db_application=null;
		if (strlen($i_db_application)){
			include_once( PORA_DIR . $i_db_application . ".class.inc" );
			$_db_application= new $i_db_application();
		}
		return  $_db_application;
	}

	function closeDb($i_tablename='',$connectionId=''){
		$session=&DbService::session($i_tablename,$connectionId);
		$session->close();
	}
	
	function closeAllDb(){
		$connections=DbService::getAllConnectionIds();
		foreach($connections as $connectionId){
			$session=&DbService::session('',$connection);
			$session->close();
		}
	}
	
	function &session($i_tablename='',$connectionId=''){
		global $CFG;
		static $_session=array();
		if(!strlen($connectionId)){
			$connectionId=DbService::getConnectionId($i_tablename);
		}
		if(!$_session[$connectionId]){
			$dbApp=&DbService::application();
			$_session[$connectionId]=$dbApp->openDb($connectionId);
			if(	($CFG->pora_dblayer=='adodb' && $_session[$connectionId]->dbh->databaseType=='mysql')
				|| $CFG->pora_dblayer=='mysql'){
				$charset_name	= $CFG->pora_connections[$connectionId][charset_name];
				$collation_name	= $CFG->pora_connections[$connectionId][collation_name];
				if($CFG->pora_dblayer=='adodb'){
					+ mysql_query("SET NAMES '$charset_name' COLLATE '$collation_name'",$_session[$connectionId]->dbh->_connectionID);
				}else{
					+ mysql_query("SET NAMES '$charset_name' COLLATE '$collation_name'",$_session[$connectionId]->dbh);
				}
			}
		}
		return  $_session[$connectionId];
	}

	function nextOID($i_tablename){
		$dbApp=&DbService::application();
		return $dbApp->nextOID($i_tablename);
	}

	function beginTransaction($i_tablename=''){
		$dbSession=DbService::session($i_tablename);
		return $dbSession->beginTransaction();
	}

	function endTransaction($i_ok,$i_tablename=''){
		$dbSession=DbService::session($i_tablename);
		return $dbSession->endTransaction($i_ok);
	}

	function install($i_deleteTables=false){
		$dbApp=&DbService::application();
		if($i_deleteTables){
			$result=$dbApp->deleteTables();
			if(PEAR::isError($result)){
				return $result;
			}
		}
		$result=$dbApp->createTables();
		if(PEAR::isError($result)){
			return $result;
		}
		$result=$dbApp->createOIDTable($i_deleteTables);
		if(PEAR::isError($result)){
			return $result;
		}
		return true;
	}

	function getConnectionId($tablename){
		global $CFG;
		if(!$CFG->pora_tables[$tablename]){
			return "default";
		}else{
			return $CFG->pora_tables[$tablename];
		}
	}
	function getAllConnectionIds(){
		global $CFG;
		$result=array();
		foreach ($CFG->pora_tables as $key=>$value) {
			if (!in_array($value,$result))
				$result[]=$value;
		}
		return $result;
	}
}

?>