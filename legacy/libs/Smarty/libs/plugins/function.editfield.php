<?php

function smarty_function_editfield($params, &$smarty){//$data,$hs_key,$class,$field,$size=10
	//print_r($params);
	$data=$params['data'];
	$hs_key=$params['hs_key'];
	$sclass=$params['sclass'];
	$field=$params['field'];
	$size=$params['size'];
	$value=$data['data'][$field];
	$name="changes[$hs_key][$field]";
	
	if( is_array($data['hideinput']) && in_array($field,$data['hideinput']) ){
		if( is_array($data['hidefield']) && in_array($field,$data['hidefield']) ){
			return "<div class=\"text\">-</div>";
		}else{
			return "<div class=\"text\">$value</div>";
		}
	}else{
		return "<input name=\"$name\" class=\"$sclass\" value=\"$value\" size=\"$size\">";
	}
}

?>