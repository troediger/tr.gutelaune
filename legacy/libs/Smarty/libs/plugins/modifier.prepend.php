<?php

function smarty_modifier_prepend($s,$prefix,$empty="")
{
	if (!empty($s) && $s!=$empty)
		return $prefix . $s;
	return "";
}

?>
