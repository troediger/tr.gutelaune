<?

class DbLink extends DbObject{

	function DbLink(){
	 	$this->DbObject("link");
	 	$this->setOID(0);
	}

	var $links_weitere="";

	function setLinks_weitere( $i_value ){
		$this->set('links_weitere',(string)$i_value );
	}

	function getLinks_weitere(){
		return $this->links_weitere;
	}

	var $links_ausgewaehlt="";

	function setLinks_ausgewaehlt( $i_value ){
		$this->set('links_ausgewaehlt',(string)$i_value );
	}

	function getLinks_ausgewaehlt(){
		return $this->links_ausgewaehlt;
	}

}
?>