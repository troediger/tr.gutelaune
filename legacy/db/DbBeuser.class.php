<?

class DbBeuser extends DbObject{

	function DbBeuser(){
	 	$this->DbObject("beuser");
	 	$this->setOID(0);
	}

	var $username="";

	function setUsername( $i_value ){
		$this->set('username',(string)$i_value );
	}

	function getUsername(){
		return $this->username;
	}

	var $password="";

	function setPassword( $i_value ){
		$this->set('password',(string)$i_value );
	}

	function getPassword(){
		return $this->password;
	}

	var $is_admin="";

	function setIs_admin( $i_value ){
		$this->set('is_admin',(string)$i_value );
	}

	function getIs_admin(){
		return $this->is_admin;
	}

}
?>