<?

class DbTour extends DbObject{

	function DbTour(){
	 	$this->DbObject("tour");
	 	$this->setOID(0);
	}

	var $titel="";

	function setTitel( $i_value ){
		$this->set('titel',(string)$i_value );
	}

	function getTitel(){
		return $this->titel;
	}

	var $datum="";

	function setDatum( $i_value ){
		$this->set('datum',(string)$i_value );
	}

	function getDatum(){
		return $this->datum;
	}

	var $uhrzeit="";

	function setUhrzeit( $i_value ){
		$this->set('uhrzeit',(string)$i_value );
	}

	function getUhrzeit(){
		return $this->uhrzeit;
	}

	var $treffpunkt="";

	function setTreffpunkt( $i_value ){
		$this->set('treffpunkt',(string)$i_value );
	}

	function getTreffpunkt(){
		return $this->treffpunkt;
	}

	var $parkmoeglichkeit="";

	function setParkmoeglichkeit( $i_value ){
		$this->set('parkmoeglichkeit',(string)$i_value );
	}

	function getParkmoeglichkeit(){
		return $this->parkmoeglichkeit;
	}

	var $wanderweg="";

	function setWanderweg( $i_value ){
		$this->set('wanderweg',(string)$i_value );
	}

	function getWanderweg(){
		return $this->wanderweg;
	}

	var $region="";

	function setRegion( $i_value ){
		$this->set('region',(string)$i_value );
	}

	function getRegion(){
		return $this->region;
	}

	var $laenge="";

	function setLaenge( $i_value ){
		$this->set('laenge',(string)$i_value );
	}

	function getLaenge(){
		return $this->laenge;
	}

	var $dauer="";

	function setDauer( $i_value ){
		$this->set('dauer',(string)$i_value );
	}

	function getDauer(){
		return $this->dauer;
	}

	var $bodenbeschaffenheit="";

	function setBodenbeschaffenheit( $i_value ){
		$this->set('bodenbeschaffenheit',(string)$i_value );
	}

	function getBodenbeschaffenheit(){
		return $this->bodenbeschaffenheit;
	}

	var $besonderheiten="";

	function setBesonderheiten( $i_value ){
		$this->set('besonderheiten',(string)$i_value );
	}

	function getBesonderheiten(){
		return $this->besonderheiten;
	}

	var $schwierigkeit="";

	function setSchwierigkeit( $i_value ){
		$this->set('schwierigkeit',(string)$i_value );
	}

	function getSchwierigkeit(){
		return $this->schwierigkeit;
	}

	var $teilnahmegebuehr="";

	function setTeilnahmegebuehr( $i_value ){
		$this->set('teilnahmegebuehr',(string)$i_value );
	}

	function getTeilnahmegebuehr(){
		return $this->teilnahmegebuehr;
	}

	var $max_teilnehmer="";

	function setMax_teilnehmer( $i_value ){
		$this->set('max_teilnehmer',(string)$i_value );
	}

	function getMax_teilnehmer(){
		return $this->max_teilnehmer;
	}

	var $hidden=0;

	function setHidden( $i_value ){
		$this->set('hidden',(int)$i_value );
	}

	function getHidden(){
		return $this->hidden;
	}

}
?>