<?

class DbBild extends DbObject{

	function DbBild(){
	 	$this->DbObject("bild");
	 	$this->setOID(0);
	}

	var $tourOID=0;

	function setTour( $i_value ){
		$this->set('tourOID',$i_value->getOID() );
	}

	function getTour(){
		$query=new LgTour;
		return $query->selectByOID($this->tourOID);
	}

	var $titel="";

	function setTitel( $i_value ){
		$this->set('titel',(string)$i_value );
	}

	function getTitel(){
		return $this->titel;
	}

	var $filename="";

	function setFilename( $i_value ){
		$this->set('filename',(string)$i_value );
	}

	function getFilename(){
		return $this->filename;
	}

	var $savepath="";

	function setSavepath( $i_value ){
		$this->set('savepath',(string)$i_value );
	}

	function getSavepath(){
		return $this->savepath;
	}

	var $sortierung=0;

	function setSortierung( $i_value ){
		$this->set('sortierung',(int)$i_value );
	}

	function getSortierung(){
		return $this->sortierung;
	}

	var $parkmoeglichkeit=0;

	function setParkmoeglichkeit( $i_value ){
		$this->set('parkmoeglichkeit',(int)$i_value );
	}

	function getParkmoeglichkeit(){
		return $this->parkmoeglichkeit;
	}

	var $treffpunkt=0;

	function setTreffpunkt( $i_value ){
		$this->set('treffpunkt',(int)$i_value );
	}

	function getTreffpunkt(){
		return $this->treffpunkt;
	}

	var $bildrotation=0;

	function setBildrotation( $i_value ){
		$this->set('bildrotation',(int)$i_value );
	}

	function getBildrotation(){
		return $this->bildrotation;
	}

}
?>