<?

require_once("db/DbTour.class.php");

class LgTour extends DbTour{

	function LgTour(){
		$this->DbTour();
	}
	

	/**
	 * Gibt eine Liste der Anhänge wieder
	 *
	 * @return array
	 */
	function getFileList(){
		$fileList=array();
		$query=new LgBild();
		$query->setTour($this);
		$options['order_by']="sortierung";
		$cursor=$query->queryByExample($options);
		while($file=$cursor->next()){
			$fileList[]=$file;
		}
		return $fileList;
	}
	
	/**
	 * Gibt die Sortierungsnummer fuer eine Neue Bilddatei wieder.
	 * Also die kleinste noch freie Sortierungsnummer für Dateien dieses Objekts.
	 * @return Int
	 */
	function getSortierungForNewFile(){
		$sorting_nr = 0;
		
		$sql = "SELECT MAX(sortierung) FROM bild WHERE tourOID = '".$this->getOID()."' ";
		
		$result = mysql_query($sql);
		$max_sorting = mysql_fetch_array($result);
		
		if($max_sorting['MAX(sortierung)']){
			$sorting_nr = $max_sorting['MAX(sortierung)']+1;
		}else{
			$sorting_nr = 1;
		}
		
		return $sorting_nr;
	}	
	
	function date_mysql2german($datum) {
	    list($jahr, $monat, $tag) = explode("-", $datum);
	    return sprintf("%02d.%02d.%04d", $tag, $monat, $jahr);
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $tour
	 * @return LgBild
	 */	
	function getVorschaubild () {
		$result=new LgBild();
	
		$query=new LgBild();
		$options['order_by']='sortierung';
		$query->setTour($this);
		$query->setTreffpunkt(0);
		$query->setParkmoeglichkeit(0);
		$cursor=$query->queryByExample($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		
		if($cursor->numRows() > 0) {
			$result = $cursor->next();
		} else {
			$query=new LgBild();
			$options['order_by']='sortierung';
			$query->setTour($this);
			$cursor=$query->queryByExample($options);
			if(PEAR::isError($cursor) ){
				die ($cursor->getMessage());
			}
			$result = $cursor->next();					
		}
		return $result;
	}
	
	function delete () {
		$bildlist = $this->getFileList();
		foreach ($bildlist as $bild) {
			$bild->delete();
		}
		// TODO Fehler noch abfangen
		$this->_table->delete();
		if(PEAR::isError($result)){
			$errors["exceptions"][]= "Datei-Datensatz konnte nicht geloescht werden: ".$result->getMessage();
			$ok=false;
		}		
	}
}

?>
