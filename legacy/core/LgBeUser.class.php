<?

require_once("db/DbBeuser.class.php");

class LgBeuser extends DbBeuser{
	
	function LgBeuser(){
		$this->DbBeuser();
	}
	
	function checkValidity(){
		$errors=array();
		if( !(strlen($this->username)>=3) )
		 	$errors["username"]="Der Benutzername ist zu kurz";
		if( !strlen($this->username) )
		 	$errors["username"]="Es wurde kein Benutzername angegeben";
		if( !(strlen($this->password)>=6) )
		 	$errors["password"]="Das Passwort ist zu kurz (min. 6 Zeichen)";
		if( !strlen($this->password) )
		 	$errors["password"]="Es wurde kein Passwort angegeben";
   	 	return $errors;

	}
}

?>
