<?

require_once("db/DbLink.class.php");

class LgLink extends DbLink{

	function LgLink(){
		$this->DbLink();
	}
	
	function readFileLinkWeitere () {
		$controller = &Controller::getInstance();
		$CFG		= &Controller::config();
		
		$errors = array();
		$ok = true;
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . "/" . $CFG->data_dir . $CFG->datei_link_weitere;
		if (file_exists($filename)){
			$rows = file($filename);
			return $rows;
		} else {
			$handle = @fopen($filename, "w+");
			if($handle) {
				$this->readFileLinkWeitere();
			} else {
				$errors['exceptions'][]="Datei kann nicht erstellt werden!";
				$ok=false;			
			}
		}
	}
	
	function readFileLinkAusgewaehlt () {
		$controller = &Controller::getInstance();
		$CFG		= &Controller::config();
			
		$errors = array();
		$ok = true;
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . "/" . $CFG->data_dir . $CFG->datei_link_ausgewaehlt;
		if (file_exists($filename)){
			$rows = file($filename);
			return $rows;
		} else {
			$handle = @fopen($filename, "w+");
			if($handle) {
				$this->readFileLinkAusgewaehlt();
			} else {
				$errors['exceptions'][]="Datei kann nicht erstellt werden!";
				$ok=false;			
			}
		}		
	}

	function saveFileLinkWeitere ($link) {
		$controller = &Controller::getInstance();
		$CFG		= &Controller::config();
			
		$filename = $_SERVER['DOCUMENT_ROOT'] . "/" . $CFG->data_dir . $CFG->datei_link_weitere;
		$errors = array();

		$ok = true;

	    if (!$handle = fopen($filename, "w")) {
			$errors['exceptions'][]="Datei kann nicht ge&ouml;ffnet werden!";
			$ok=false;	
    	}

		for($i=0; $i<count($link); $i++) {
			if (!fwrite($handle, $link[$i])) {
				$errors['exceptions'][]="Datei kann nicht beschrieben werden!";
				$ok=false;
			}			
		}
	}
	
	function saveFileLinkAusgewaehlt ($link) {
		$controller = &Controller::getInstance();
		$CFG		= &Controller::config();
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . "/" . $CFG->data_dir . $CFG->datei_link_ausgewaehlt;
		$errors = array();
		$ok = true;

	    if (!$handle = fopen($filename, "w")) {
			$errors['exceptions'][]="Datei kann nicht ge&ouml;ffnet werden!";
			$ok=false;	
    	}

		for($i=0; $i<count($link); $i++) {
			if (!fwrite($handle, $link[$i])) {
				$errors['exceptions'][]="Datei kann nicht beschrieben werden!";
				$ok=false;
			}			
		}		
	}	
}

?>
