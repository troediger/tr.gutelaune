<?php

class LgCaptcha {

	/**
	 * erste Zahl der Rechnung, minimaler Wert, VON
	 * 
	 * @var int $minZahl1
	 */
	private $minZahl1 = 11;

	/**
	 * erste Zahl der Rechnung, maximaler Wert, BIS
	 * 
	 * @var int $maxZahl1
	 */	
	private $maxZahl1 = 20;

	/**
	 * zweite Zahl der Rechnung, minimaler Wert, VON
	 * 
	 * @var int $maxZahl1
	 */		
	private $minZahl2 = 1;
	
	/**
	 * zweite Zahl der Rechnung, maximaler Wert, BIS
	 * 
	 * @var int $maxZahl1
	 */		
	private $maxZahl2 = 10;
	
	private $rechen_captcha_spam = null;
	

	/**
	 * vergleicht den eingetragenen Wert mit dem in der Session gespeicherten
	 * 
	 * @param string $sicherheitscode
	 * @return bool $result true/false
	 */
	public function isCaptcha($sicherheitscode) {
		$result = false;
		$sicherheits_eingabe = str_replace("=", "", $sicherheitscode);
		if(isset($this->rechen_captcha_spam) AND $sicherheits_eingabe == $this->rechen_captcha_spam) {
			unset($this->rechen_captcha_spam);
			$result = true;
		}
		return $result; 
	}	
	
	/**
	 * gibt eine Farb-ID, die durch die angegebenen RGB-Werte bestimmt wird, zurueck
	 * 
	 * @param string $img
	 * @param string $hexstr
	 * 
	 * @return array
	 */
	public function ImageColorAllocateFromHex ($img, $hexstr) {
		$int = hexdec($hexstr);
		return ImageColorAllocate (
			$img,
			0xFF & ($int >> 0x10),
			0xFF & ($int >> 0x8),
			0xFF & $int);
	}
	
	/**
	 * gibt das Captcha zurueck
	 */
	public function showCaptcha() {

		$zahl1 = rand($this->minZahl1, $this->maxZahl1);
		$zahl2 = rand($this->minZahl2, $this->maxZahl2);
		$operator = rand(1, 2); // + oder -
		if($operator == "1") {
			$operatorzeichen = " + ";
			$ergebnis = $zahl1 + $zahl2;
		} else {
			$operatorzeichen = " - ";
			$ergebnis = $zahl1 - $zahl2;
		}
		$this->rechen_captcha_spam = str_replace("=", "", $ergebnis);
		$rechnung = $zahl1 . $operatorzeichen . $zahl2 . " =";
		$img = imagecreatetruecolor(80, 15);
		$schriftfarbe = $this->ImageColorAllocateFromHex($img, '003300');
		$hintergrund = $this->ImageColorAllocateFromHex($img, DEF2C4);
		
		imagefill($img, 0, 0, $hintergrund);
		imagestring($img, 3, 2, 0, $rechnung, $schriftfarbe);
		header("Content-type: image/png");
		imagepng($img);
		imagedestroy($img);		
		exit();			
	}	
}

?>
