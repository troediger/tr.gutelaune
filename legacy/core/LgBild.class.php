<?

require_once("db/DbBild.class.php");

class LgBild extends DbBild{

	function LgBild(){
		$this->DbBild();
	}
	
	
	function getFileEnding(){
		$ending=substr($this->getFilename(),strrpos($this->getFilename(),".")+1);
		return $ending;
	}
	
	function getMimeType(){
		include("mime.php");
		
		$ending=$this->getFileEnding();
		
		if( strlen($mimetypes[$ending]) )
			return $mimetypes[$ending];
		
		return "application/octet-stream";
	}
	
	function isImage(){
		$mimeType = $this->getMimeType();
		if(preg_match("/image/i", $mimeType)){
			return true;
		}else{
			return false;
		}
	}
	
	function isVideo(){
		$mimeType = $this->getMimeType();
		if(preg_match("/video/i", $mimeType)){
			return true;
		}else{
			return false;
		}
	}
	
	function isPdf(){
		$mimeType = $this->getMimeType();
		if(preg_match("/pdf/i", $mimeType)){
			return true;
		}else{
			return false;
		}
	}
	
	function saveFile($i_from){
		
		$target=$_SERVER['DOCUMENT_ROOT'] . "/" . $this->getSavepath();
		$errors=array();

		$ok=true;

		if(!strlen($this->getSavepath()) || !file_exists( $target ) ){
			$errors['exceptions'][]="Zielpfad  ist leer oder existiert nicht";
			$ok=false;
		}
		if(!strlen($this->getFilename()) ){
			$errors['exceptions'][]="Kein Dateiname angegeben";
			$ok=false;
		}
		if(!$this->tourOID ){
			$errors['exceptions'][]="Datei ist nicht mit einem Objekt verknüpft";
			$ok=false;
		}

		if($ok){
			/*
			if( !copy( $i_from, $target . "/" . $this->getOID() ) ){
				$errors['exceptions'][]="Datei konnte nicht kopiert werden.";
				$ok=false;
			}else{	
				if ($this->OID){
					$result=$this->update();
				}else{
					$result=$this->insert();
				}
			}
			*/
			//Alte Funktion benutzt Transaktion
			if ($this->OID){
				$result=$this->update();
			}else{
				$result=$this->insert();
			}
			if(PEAR::isError($result)){
				$errors["exceptions"][]="Datei konnte nicht gespeichert werden: ".$result->getMessage();
				$ok=false;
			}

			if( !copy( $i_from, $target . "/" . $this->getOID() ) ){
				$errors['exceptions'][]="Datei konnte nicht kopiert werden.";
				$ok=false;
			}
			
		}
		return $errors;
	}
	
	function delete(){

		$path=$_SERVER['DOCUMENT_ROOT'] . "/" . $this->getSavepath();
		$errors=array();

		$ok=true;
		
		if(!strlen($this->getSavepath()) || !file_exists( $path ) ){
			$errors['exceptions'][]="Zielpfad  ist leer oder existiert nicht";
			$ok=false;
		}
		
		if(!@unlink($path . $this->getOID())){
			$errors['exceptions'][]="Datei konnte nicht geloescht werden";
			$ok=false;
		}
		if($ok){
			$result=$this->_table->delete();
			if(PEAR::isError($result)){
				$errors["exceptions"][]= "Datei-Datensatz konnte nicht geloescht werden: ".$result->getMessage();
				$ok=false;
			}
			
		}
		return $errors;
		
	}
	
	function getThumbnailFilename($max_x, $max_y){
		$controller = &Controller::getInstance();
		$CFG		= &Controller::config();

		if($this->isImage()){
			$thumbnail_path	= $CFG->thumbnail_path;
			$base_dir		= $CFG->base_dir;
			
			$file_ext		= strtolower(substr($this->getFilename(),strrpos($this->getFilename(),".")+1));
			$filename		= $thumbnail_path . $this->getOID() . '_' . $max_x . '_' . $max_y . '.' . $file_ext;
			
			if(!file_exists($base_dir . $filename)){
				$this->generateThumbnail($this->getSavepath(),$this->getOID(),$filename, $max_x, $max_y);
			}
				
			return $filename;
		}else{
			return false;
		}
	}


	// Generiert skalierte Kopien der Motive ($image_path/$image_name) und
	// legt diese im angebenen Verzeichnis $thumb_path unter $size_$image_name ab
	function generateThumbnail($image_path,$image_name, $thumbnail_file, $max_x, $max_y){
		$controller = &Controller::getInstance();
		$CFG		= $controller->config();

		$base_dir		= $CFG->base_dir;
		//$convert_path	= $CFG->imagemagick_convert_path;
		//$identify_path	= $CFG->imagemagick_identify_path;
		
		list($image_x, $image_y, $aaa, $bbb) = getimagesize($image_path . $image_name);
		
		$factor_max_x = $max_x / $image_x;
		$factor_max_y = $max_y / $image_y;
				
		if($factor_max_x < $factor_max_y){
			$factor = $factor_max_x;
		}else{
			$factor = $factor_max_y;
		}
		
		$dest_img_x		= $image_x * $factor;
		$dest_img_y		= $image_y * $factor;
		
		$file_ext = strtolower(substr($this->getFilename(),strrpos($this->getFilename(),".")+1));

		switch($file_ext){
			case 'gif':	
				$src_img = imagecreatefromgif($image_path . $image_name);
				break;
			case 'JPG':
			case 'jpg':	
			case 'jpeg':	
				$src_img = imagecreatefromjpeg($image_path . $image_name);
				break;
			case 'png':	
				$src_img = imagecreatefrompng($image_path . $image_name);	
				break;	
			default:
				echo "image format $file_ext is not supported";
				die();
		}
		
		$dest_img = imagecreatetruecolor($dest_img_x, $dest_img_y);
		
		// Schnell aber schlechte Qualität
		//imagecopyresized($dest_img, $src_img, 0, 0, 0, 0, $dest_img_x, $dest_img_y , $image_x, $image_y);
		
		// Langsamer aber gute Qualität
		imagecopyresampled($dest_img, $src_img, 0, 0, 0, 0, $dest_img_x, $dest_img_y , $image_x, $image_y);

		switch($file_ext){
			case 'gif':	
				imagegif($dest_img, $thumbnail_file);
				break;
			case 'JPG':
			case 'jpg':	
			case 'jpeg':	
				imagejpeg($dest_img, $thumbnail_file);
				break;
			case 'png':	
				imagepng($dest_img, $thumbnail_file);
				break;	
			default:
				echo "image format $file_ext is not supported";
				die();
		}
		
		return true;
	}
	
	/**
	 * Prueft ob zu diesem Objekt auch die Bilddatei vorhanden ist.
	 *
	 * @return BOOL
	 */
	function file_really_exists(){
		$path=$_SERVER['DOCUMENT_ROOT'] . "/" . $this->getSavepath();
		
		if(file_exists($path . $this->getOID())){
			return true;
		}
		return false;
	}	
	
	/**
	 * liefert die Bildtitel fuer die Rotation
	 * 
	 * @param 
	 * @return Array ($bildTitel)
	 */	
	function getBildTitel ($bildTitel) {
		$bildTitel = preg_replace("/_/"," ", $bildTitel);
		return $bildTitel;
	}	

	/**
	 * liefert die Bildtitel ohne Endung aus dem Dateinamen
	 * 
	 * @param 
	 * @return Array ($bildTitel)
	 */	
	function getBildTitelohneEndung ($bildTitel) {
		$bildTitel = preg_replace(array("/.jpg/", "/.gif/", "/.jpeg/", "/.bmp/", "/.png/", "/.JPG/"),array("", "", "", "", "", ""), $bildTitel);
		return $bildTitel;
	}		
	
}

?>
