<?

class Controller {
	
	var $beuser=null;
	var $feuser=null;
	var $visit_counted=false;
	var $is_logged_in=false;
	var $tour=null;
	var $last_search;
	var $captcha=null;
	//var $backURL=array();
	//var $redirect=array();
	
	function Controller(){
	}
	
	function &getInstance(){
		if (!isset($_SESSION["controller"])){
			$_SESSION["controller"]=new Controller();
		}
		return $_SESSION["controller"];
	}
	
	function captcha(){
		if(!$this->captcha) {
			$this->captcha = new LgCaptcha();
		}
		return $this->captcha;
	}
	function loadClasses(){
		require_once("core/LgBeUser.class.php");
		require_once("core/LgTour.class.php");
		require_once("core/LgBild.class.php");
		require_once("core/LgLink.class.php");
		require_once("core/LgCaptcha.class.php");
	}
	
	function count($tour_id,$page){
		LgStatistik::count($tour_id,$page);
		if($page=="start" && !$this->visit_counted){
			LgStatistik::count($tour_id,"VISIT");
			$this->visit_counted=true;
		}
	}
	
	function connect(){
		$r=DbService::application("DbVmFrameworkApplication");
		if(PEAR::isError($r))
		{
			return $r;
		}
		return DbService::session();
	}
	
	
	function &config(){
		global $CFG;
		return $CFG;
	}

	function SID(){
		static $_session_id="";
		if(!strlen( $_session_id)){
			$_session_id=session_id();
		}
		return $_session_id;
	}
	
	function trimArray(&$array){
		foreach ($array as $key=>$value){
			if(!is_array($value))
			$array[$key]=trim($value);
		}
	}
	
	function toEnFloat($i_float){
		$i_float=preg_replace("/\./","",$i_float);
		$i_float=preg_replace("/\,/",".",$i_float);
		return (double)$i_float;
	}
	
/*	
	function &tour($tour_id=""){
		if(!$this->tour || strlen($tour_id)){
			$CFG=Controller::config();
			if( !strlen( $tour_id ) ){
				$tour_id=$CFG->default_tour_id;
			}
			$query=new LgTour();
			if (PEAR::isError($query->_error))
				die($query->getMessage());
			$query->setTitel($tour_id);
			$cursor=$query->queryByExample();
/*			
			if(!$cursor->numRows()){
				$query->setTitel($CFG->default_tour_id);
				$cursor=$query->queryByExample();
				if (!$cursor->numRows())
					die("keine Tour definiert");

			}
			$this->tour=$cursor->next();

		}
		return $this->tour;
	}
*/	
/*	
	function &skin(&$i_form){
		
		static $skin=null;
		if(!$skin){
			$tour=&$this->tour();
			$skin=$tour->getSkin();
			$skin['appwidth']-=2;
		}
		return $skin;
	}
*/	
	
	function login(&$i_form){
		$query=new LgBeUser();
		Controller::trimArray($i_form);
		$isEmpty=false;
		
		if( strlen($i_form['password']) ){
			$query->setPassword($i_form['password']);
		}else{
			$isEmpty=true;
		}
		
		if( strlen($i_form['username']) ){
			$query->setUsername($i_form['username']);
		}else{
			$isEmpty=true;
		}
		
		
		if(!$isEmpty){
			$r=$query->queryByExample();
			$beuser=null;
			if(!PEAR::isError($r))
			$beuser=$r->next();
			if(!PEAR::isError($beuser)){
				if($beuser!=null){
					$this->beuser=$beuser;
					$this->is_logged_in=true;
				}
			}
		}
	}
	
	function logout(&$i_form){
		$this->user=null;
		$this->is_logged_in=false;
		
	}	
	
	function is_admin(){
		if ($this->beuser->getIs_admin()=="ja")
			return true;
		return false;
	}
	
	function errorsToMessageList($i_errors){
		$errormsgList=array();
		if(is_array($i_errors)) {
			foreach ($i_errors as $error){
				if (PEAR::isError($error)) {
					$errormsgList[]=$error->getMessage();
				}
				if (is_array($error)) {
					$errormsgList=array_merge($errormsgList,Controller::errorsToMessageList($error));
				}
				if (is_string($error)) {
					$errormsgList[]=$error;
				}
			}			
		}

		return $errormsgList;
	}

}

?>