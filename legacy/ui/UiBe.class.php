<?
define("SMARTY_DIR",$CFG->smarty_dir);
require( SMARTY_DIR . "Smarty.class.php" );

class UiBe{

	function processRequest(&$i_form){
		$CFG=Controller::config();
		$action=UiBe::getAction($i_form);
		$page=$i_form["page"];
		$processor=UiBe::getProcessor($page,$action);
		$smarty=&UiBe::smarty();
		$smarty->assign("SID",session_id());
		$controller=&UiBe::controller();

		$ok=false;
		
		if( $controller->is_logged_in==true || $controller->user != null ){
			list($class,$method)=explode("::",$processor);
			if ( strlen($class) && strlen($method) ) {
				eval("\$ok=$processor(\$i_form);");
			}
			if($ok){
				$controller=&Controller::getInstance();
				$smarty->assign("ERRORS",$GLOBALS['ERRORS']);
				$smarty->assign("WARNINGS",$GLOBALS['WARNINGS']);
				$smarty->display("be_body.html");
			}
		}else{
		   if($i_form['page']=="login"){
				$i_form['page']="";
				$controller->login($i_form);
				UiBe::processRequest(&$i_form);
			}else{
				$smarty->assign("form",$i_form);
				$smarty->display("be_login.html");
			}
		}
		return;
	}

	function getProcessor(&$i_page,$i_action=""){
		$CFG=Controller::config();
		include_once( $CFG->ui_dir . "UiBeApplication.class.php" );
		switch($i_page){
			case "tour":
				if ($i_action=="save"){
					return "UiBeApplication::getTourSavePage";
				}
				if ($i_action=="delete"){
					return "UiBeApplication::getTourDeletePage";
				}
				return "UiBeApplication::getTourPage";
				break;
			case "link":
				if ($i_action=="save"){
					return "UiBeApplication::getLinkSavePage";
				}
				return "UiBeApplication::getLinkPage";
				break;				
			case "benutzer":
				if ($i_action=="save"){
					return "UiBeApplication::getBenutzerSavePage";
				}
				if ($i_action=="delete"){
					return "UiBeApplication::getBenutzerDeletePage";
				}
				return "UiBeApplication::getBenutzerPage";
				break;				
			case "logout":
				return "UiBeApplication::getLogoutPage";
				break;				
			default:
				$i_page="tour";
				return "UiBeApplication::getTourPage";
				break;
		}
	}
	
	function getAction(&$i_form){
		if( isset($i_form["action"])  ){
			return $i_form["action"];
		}elseif(isset($i_form["action_add"])){
			$i_form['action']='add';
			return "add";
		}elseif(isset($i_form["action_delete"])){
			$i_form['action']='delete';
			return "delete";			
		}elseif(isset($i_form["action_edit"])){
			$i_form['action']='edit';
			return "edit";
		}elseif( isset($i_form["action_save"]) ){
			$i_form['action']='save';
			return "save";
		}
		return "";
		
	}
	
	function &smarty(){
		global $CFG;
		static $smarty=null;
		if(!$smarty){
			$smarty=new Smarty();
			$smarty->template_dir=$CFG->smarty_template_dir;
			$smarty->compile_dir=$CFG->smarty_compile_dir;
			$smarty->config_dir = $CFG->smarty_config_dir;
			$smarty->debugging=$CFG->smarty_debug;
			$smarty->debug_tpl=$CFG->smarty_debug_tpl;
		}
		return $smarty;
	}

	function &controller(){
		return Controller::getInstance();
	}

	function &cache($group="default"){
		global $CFG ;

		$controller=&UiBe::controller();
		$cache=&Controller::cache($group);
		return $cache;
	}

	
}
?>