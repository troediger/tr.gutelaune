<?
class UiBeApplication{


####################### Touren ####################################
	
	function getTourPage(&$i_form){
		$smarty=&UiBe::smarty();
		$controller=&Controller::getInstance();
		$CFG=$controller->config();

		// Alle Touren laden
		$tourList=array();
		
		$fileList=array();
		
		$query=new LgTour();
		$options['order_by']='datum DESC';
		$cursor=$query->selectAll($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		while ($result=$cursor->next()){
			$tourList[]=$result;
		}

		// Zu bearbeitenden Tour laden
		if ($i_form['sel_tourOID']){
			$query=new LgTour();
			if ($i_form['action_add']){
				$tour=$query;
			}else{
				$tour=$query->selectByOID($i_form['sel_tourOID']);
				if($tour->getOID()){
					$fileList=$tour->getFileList();
				}				
			}
		}
		
		// falls die Bearbeitungsmaske nach einer Fehlerhaften Eingabe angezeigt werden soll, 
		// werden die Formulardaten zugewiesen
		if (isset($i_form['OID']) && sizeof($i_form['errors'])){
			UiBeApplication::setObjectvalues($tour,$i_form);
		}
		
		// Rettungsanker: beim aufruf via "Default" Seite ueber die UiBe
		// ist Page leer. Muss jedoch immer eine Tour sein,
		// damit diese Seite richtig dargestellt wird.
		if($i_form['page'] == ''){
			$i_form['page'] = 'tour';
		}		
		
		$smarty->assign("form",$i_form);
		$smarty->assign("tourList",$tourList);
		$smarty->assign("fileList",$fileList);
		$smarty->assign("tour",$tour);
		$smarty->assign("content","be_tour.html");
		return true;
	}

	function getTourSavePage($i_form){
		global $CFG;
		$smarty=&UiBe::smarty();
		$controller=&Controller::getInstance();

		if ($i_form['OID']){
			$query=new LgTour();
			$tour=$query->selectByOID($i_form['OID']);
		}else{
			$tour=new LgTour();
		}
		
		UiBeApplication::setObjectvalues($tour,$i_form);
		
		$errors = $tour->save();
		$errorList=Controller::errorsToMessageList($errors);

		// Speichern Falls keine Fehler aufgetreten sind
		if(sizeof($errorList) == 0){
			$i_form['errors'] = $errorList;
			$i_form['tourOID']=$tour->getOID();
			$i_form['sel_tourOID']=$tour->getOID();
			
			// Bildtexte & Sortierung speichern
			if(is_array($i_form['bild_metadaten'])){
				foreach($i_form['bild_metadaten'] as $bildOID => $metadaten){
					$query = new LgBild();
					$bild = $query->selectByOID($bildOID);
					if(!PEAR::isError($bild)){
						$bild->setTitel(LgBild::getBildTitel($metadaten['titel']));
						$bild->setTreffpunkt($metadaten['treffpunkt']);
						$bild->setParkmoeglichkeit($metadaten['parkmoeglichkeit']);
						$bild->setBildrotation($metadaten['bildrotation']);
						$bild->setSortierung($metadaten['sortierung']);
						$bild->save();
					}
				}
			}
			
			// Angehaengte Bilder speichern
			if( sizeof($_FILES['img']) ){
				foreach ($_FILES['img']['tmp_name'] as $n=>$tmp_file){
					if(strlen($tmp_file)){
						$bild=new LgBild();
						$bild->setTour($tour);
						$bild->setTitel(LgBild::getBildTitel(LgBild::getBildTitelohneEndung($_FILES['img']['name'][$n])));
						$bild->setFilename($_FILES['img']['name'][$n]);
						$bild->setSavepath($CFG->img_dir);
						$bild->setSortierung($tour->getSortierungForNewFile());
						$r=$bild->saveFile($tmp_file);
						if(is_array($r)){
							$errorList=array_merge($errorList,Controller::errorsToMessageList($r));
						}
					}
				}
			}

			// ggf. Bild loeschen
			if(is_array($i_form['bild_loeschen'])){
				foreach($i_form['bild_loeschen'] as $bildOID => $loeschen){
					$query = new LgBild();
					$bild = $query->selectByOID($bildOID);
					if(!PEAR::isError($bild)){
						$bild->delete();
					}
				}
			}			

		// Ansonsten zurueck zu Eingabemaske
		}else{
			$i_form['errors'] = $errorList;
			if($i_form['tourOID']){
				$i_form['action_edit'] = "Bearbeiten";
			}else{
				$i_form['action_add'] = "Neu";
			}
		}
		return UiBeApplication::getTourPage($i_form);
	}

	function getTourDeletePage(&$i_form){

		if ($i_form['sel_tourOID']){
			$query=new Lgtour();
			$tour=$query->selectByOID($i_form['sel_tourOID']);
			
			if($tour && !PEAR::isError($tour)){
				$tour->delete();
			}

			unset($i_form['sel_tourOID']);
		}
		
		return UiBeApplication::getTourPage($i_form);
	}	
	
####################### Links ####################################	

	function getLinkPage(&$i_form){
		$smarty=&UiBe::smarty();
		$controller=&Controller::getInstance();
		$CFG=$controller->config();

		$read = new LgLink();
		$link_ausgewaehlt = $read->readFileLinkAusgewaehlt();		
		$link_weitere = $read->readFileLinkWeitere();		
		
		$smarty->assign("form",$i_form);
		$smarty->assign("link_ausgewaehlt",$link_ausgewaehlt);
		$smarty->assign("link_weitere",$link_weitere);
		$smarty->assign("content","be_link.html");
		return true;
	}

	
	function getLinkSavePage(&$i_form){
		
		$link_weitere = preg_split("/\s{2,}/", $i_form['link_weitere']);
		$link_ausgewaehlt = preg_split("/\s{2,}/", $i_form['link_ausgewaehlt']);

		for($i=0; $i<count($link_weitere); $i++) {
		    if ($link_weitere[$i]== "") {
		    	unset ($link_weitere[$i]);
		    } else {
				$link_weitere[$i] = preg_replace("/http:\/\//", "", $link_weitere[$i]);
				$link_weitere[$i].= "\r\n";		    			
		    }
		}
		for($i=0; $i<count($link_ausgewaehlt); $i++) {
			if ($link_ausgewaehlt[$i]== "") {
		    	unset ($link_ausgewaehlt[$i]);
			} else {
				$link_ausgewaehlt[$i] = preg_replace("/http:\/\//", "", $link_ausgewaehlt[$i]);
				$link_ausgewaehlt[$i].= "\r\n";				
			}
		}
		
		$write = new LgLink();
		$write->saveFileLinkWeitere($link_weitere);
		$write->saveFileLinkAusgewaehlt($link_ausgewaehlt);

		return UiBeApplication::getLinkPage($i_form);
	}	
	
####################### Benutzer ####################################	
	
	function getBenutzerPage(&$i_form){
		$smarty=&UiBe::smarty();
		$controller=&Controller::getInstance();
		$CFG=$controller->config();

		// Aller Benutzer laden
		$beuserList=array();
		$query=new LgBeuser();
		$cursor=$query->selectAll();
		while ($result=$cursor->next()){
			$beuserList[]=$result;
		}
		// Zu bearbeitenden Benutzer laden
		if ($i_form['sel_beuserOID']){
			$query=new Lgbeuser();
			if ($i_form['action_add']){
				$beuser=$query;
			}else{
				$beuser=$query->selectByOID($i_form['sel_beuserOID']);
			}
		}
		// falls die Bearbeitungsmaske nach einer Fehlerhaften Eingabe angezeigt werden soll, 
		// werden die Formulardaten zugewiesen
		if (isset($i_form['OID']) && sizeof($i_form['errors'])){
			UiBeApplication::setObjectvalues($beuser,$i_form);
		}
		$smarty->assign("form",$i_form);
		$smarty->assign("beuserList",$beuserList);
		$smarty->assign("beuser",$beuser);
		$smarty->assign("content","be_benutzer.html");
		return true;
	}

	
	function getBenutzerSavePage(&$i_form){
		if ($i_form['OID']){
			$query=new Lgbeuser();
			$beuser=$query->selectByOID($i_form['OID']);
		}else{
			$beuser=new Lgbeuser();
		}
		
		$beuser->setUsername($i_form['username']);
		$beuser->setPassword($i_form['password']);
		$beuser->setIs_admin($i_form['is_admin']);
		
		$errors = $beuser->save();
		$errorList=Controller::errorsToMessageList($errors);
		
		// Speichern Falls keine Fehler aufgetreten sind
		if(sizeof($errorList) == 0){
			$i_form['errors'] = $errorList;
			$i_form['OID']=$beuser->getOID();
			$i_form['sel_beuserOID']=$beuser->getOID();
		// Ansonsten zurueck zu Eingabemaske
		}else{
			$i_form['errors'] = $errorList;
			if($i_form['OID']){
				$i_form['action_edit'] = "Bearbeiten";
			}else{
				$i_form['action_add'] = "Neu";
			}
		}
		return UiBeApplication::getBenutzerPage($i_form);
	}

	/**
	 * loescht einen Benutzer
	 *
	 * @return true
	 */	
	function getBenutzerDeletePage(&$i_form){
		if ($i_form['sel_beuserOID']){
			$query=new Lgbeuser();
			$beuser=$query->selectByOID($i_form['sel_beuserOID']);
			if($beuser && !PEAR::isError($beuser)){
				$beuser->delete();
			}
			unset($i_form['sel_beuserOID']);
		}
		return UiBeApplication::getBenutzerPage($i_form);
	}	
	
	/**
	 * Meldet den Benutzer ab.
	 *
	 * @return true
	 */
	function getLogoutPage(){
		$smarty=&UiBe::smarty();
		$controller=&Controller::getInstance();

		$controller->logout($i_form);
		
		header('Location: be.php');
		flush;
		return true;
	}	
	
	/**
	 * Weist die Formularwerte einem Objekt. Hierbei werden alle Werte des Formulars ausser acht gelassen, 
	 * die nicht Objektattribute sind. Achtung. Formularwerte ind denenn "OID" vorkommt werden ebenfalls nciht übernommen
	 *
	 * @param DbObject $object
	 * @param Array $i_form
	 */
	function setObjectvalues(&$object,&$i_form){
		$result=array();
		$objectvalues=$object->getAllProperties();
		
		foreach ($objectvalues as $key=>$value){
			if(strpos($key,"OID")===false)
				$result[$key]=$i_form[$key];
		}
		
		$object->setProperties($result);
		flush();
	}
}
?>