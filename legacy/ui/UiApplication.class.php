<?php

/**
 * Projekt:     VmFramework Demo-App
 * File:        UiApplication.class.php
 *
 * Beispielanwendung zur Demonstration der Architektur der Vipermedia-Tools
 *
 * @copyright 1999-2005 ViperMedia M.Neumann 6 T.Roediger GbR
 * @author Thomas Roediger <t.roediger@vipermedia.de>
 * @package VmFramework.Core
 */

/**
 * UiApplikation enthaelt alle Funktionen um Nutzeraktionen zu
 * behandeln und die Ausgabe vorzubereiten.
 *
 * @package VmFramework.Ui
 */
class UiApplication{
	
	/**
	 *@param 
	 */
	function getStart_Page(&$i_form){
		global $CFG;
		$smarty=&Ui::smarty();
		$controller=&Controller::getInstance();
		
		// zuweisen der Templatevariablen
		$smarty->assign("menue_item","startseite");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Startseite");
		
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());
		
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());

		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		
		$smarty->assign("form",$i_form);
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_content.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");		
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");
		
		return true;
	}
	
	function getAktuelleTourDetailPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		// Tour laden
		//$aktuelleTour = UiApplication::getAktuelleTour();				
		if($aktuelleTour = UiApplication::getAktuelleTour()) {
		
			$bilderParken=array();
			$bilderTreff=array();
			
			$query=new LgBild();
			$query->setTour($aktuelleTour);
			$query->setParkmoeglichkeit(1);
			
			$cursor=$query->queryByExample();
			while ($result=$cursor->next()) {
				$bilderParken[]=$result;
			}
			
			$query=new LgBild();
			$query->setTour($aktuelleTour);
			$query->setTreffpunkt(1);
			
			$cursor=$query->queryByExample();
			while ($result=$cursor->next()) {
				$bilderTreff[]=$result;
			}
		}

		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Aktuelle Tour");
		
		if(count($aktuelleTour) > 0) {
			$smarty->assign("menue_item","aktuelle_tour");
			$smarty->assign("aktuelleTour",$aktuelleTour);
			$smarty->assign("bilderparken",$bilderParken);
			$smarty->assign("bildertreff",$bilderTreff);
			$smarty->assign("content","fe_aktuelle_tour_detail.html");
		} else {
			$smarty->assign("menue_item","aktuelle_tour_keine");
			$smarty->assign("content","fe_aktuelle_tour_detail_keine_tour.html");
		}
		
		$smarty->assign("form",$i_form);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());		
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");		
		
		return true;
		
	}
	
	function getAktuelleTourFormPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();

		// Tour laden
		$query = new LgTour();
		$query->setOID($i_form['OID']);
		$cursor=$query->queryByExample($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		$aktuelleTour=$cursor->next();		
		
		$smarty->assign("menue_item","aktuelle_tour_form");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Anmeldung Aktuelle Tour");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("aktuelleTour",$aktuelleTour);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());				
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		
		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_aktuelle_tour_form.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");
		
		return true;
	}	
	
	function getAktuelleTourFormCommitPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		$smarty->assign("menue_item","aktuelle_tour_form");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Anmeldung Aktuelle Tour");
		
		// ggf. Formular verarbeiten
		$errors = array();
		if($i_form['aktuelle_tour_formular_name'] == '') {
			$errors['name'] = true;
		}
		
		if($i_form['aktuelle_tour_formular_vorname'] == '') {
			$errors['vorname'] = true;	
		}
		
		if(($i_form['aktuelle_tour_formular_email'] == '' || !strpos($i_form['aktuelle_tour_formular_email'], '@')) &&  
			$i_form['aktuelle_tour_formular_telefon'] == '') {
			$errors['email'] = true;
			$errors['telefon'] = true;
		}
		
		$captcha = $controller->captcha();
					
		if(!$captcha->isCaptcha($i_form['aktuelle_tour_formular_sicherheitscode'])) {
			$errors['sicherheitscode'] = true;	
		}		
		
		// wenn die Eingaben fehlerhaft waren wird auf die Eingabeseite verwiesen, 
		// anderenfalls, wird die E-Mail versendet und eine Erfolsmeldung ausgegeben.
		if (sizeof($errors)){
			$errors['hinweis'] =  '<p class="fehler">Bitte f&uuml;lle die rot markierten Felder korrekt aus.</p>';
			$i_form['errors'] = $errors;
			return UiApplication::getAktuelleTourFormPage($i_form);
		} else {
			// Tour laden
			$query = new LgTour();
			$query->setOID($i_form['OID']);
			$cursor=$query->queryByExample();
			if(PEAR::isError($cursor) ){
				die ($cursor->getMessage());
			}
			$aktuelleTour=$cursor->next();			
			
			$i_form['aktuelle_tour_name'] = $aktuelleTour->getTitel();  
			$i_form['aktuelle_tour_datum'] = LgTour::date_mysql2german($aktuelleTour->getDatum());
			
			UiApplication::sendTourAnmeldungMail($i_form);	

			$smarty->assign("form",$i_form);
			$smarty->assign("rotation",UiApplication::getBildrotation());
			$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
			$smarty->assign("link_weitere",UiApplication::getLinkWeitere());						
//			$smarty->assign("tourheader",UiApplication::getAktuelleTour());

			$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
			$smarty->assign("email_body",$CFG->weiterempfehlen['body']);			
			
			$smarty->assign("hauptmenue","fe_hauptmenue.html");
			$smarty->assign("content","fe_aktuelle_tour_form_commit.html");
			$smarty->assign("headertop","fe_headertop.html");
			$smarty->assign("bilderrotation","fe_bildrotation.html");
			$smarty->assign("teaser","fe_teaser.html");
			$smarty->assign("seitenbild","fe_seitenbild.html");
			$smarty->assign("footermenue","fe_footermenue.html");
			return true;
		}
	}


    function getKursePage(&$i_form){
        global $CFG;
        $smarty		= &Ui::smarty();
        $controller	= &Ui::controller();

        $smarty->assign("email_schnupperkurse",$CFG->email['schnupperkurse']);
        $smarty->assign("email_fitindenfruehling",$CFG->email['fitindenfruehling']);
        $smarty->assign("email_frischeluft",$CFG->email['frischeluft']);

        $smarty->assign("menue_item","kurse");
        $smarty->assign("title_tag","GuteLauneWalkerBerlin - Kurse");

        $smarty->assign("form",$i_form);
        $smarty->assign("rotation",UiApplication::getBildrotation());
        $smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
        $smarty->assign("link_weitere",UiApplication::getLinkWeitere());
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
        $smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
        $smarty->assign("email_body",$CFG->weiterempfehlen['body']);
        $smarty->assign("hauptmenue","fe_hauptmenue.html");
        $smarty->assign("content","fe_kurse.html");
        $smarty->assign("headertop","fe_headertop.html");
        $smarty->assign("bilderrotation","fe_bildrotation.html");
        $smarty->assign("teaser","fe_teaser.html");
        $smarty->assign("seitenbild","fe_seitenbild.html");
        $smarty->assign("footermenue","fe_footermenue.html");

        return true;
    }
    function getFewoPage(&$i_form){
        global $CFG;
        $smarty		= &Ui::smarty();
        $controller	= &Ui::controller();

        $smarty->assign("email_schnupperkurse",$CFG->email['schnupperkurse']);
        $smarty->assign("email_fitindenfruehling",$CFG->email['fitindenfruehling']);
        $smarty->assign("email_frischeluft",$CFG->email['frischeluft']);

        $smarty->assign("menue_item","fewo");
        $smarty->assign("title_tag","GuteLauneWalkerBerlin - Ferienwohnung");

        $smarty->assign("form",$i_form);
        $smarty->assign("rotation",UiApplication::getBildrotation());
        $smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
        $smarty->assign("link_weitere",UiApplication::getLinkWeitere());
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
        $smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
        $smarty->assign("email_body",$CFG->weiterempfehlen['body']);
        $smarty->assign("hauptmenue","fe_hauptmenue.html");
        $smarty->assign("content","fe_fewo.html");
        $smarty->assign("headertop","fe_headertop.html");
        $smarty->assign("bilderrotation","fe_bildrotation.html");
        $smarty->assign("teaser","fe_teaser.html");
        $smarty->assign("seitenbild","fe_seitenbild.html");
        $smarty->assign("footermenue","fe_footermenue.html");

        return true;
    }


    function getTourenarchivPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();

		// Alle Touren laden
		$tourList = array();
		
		$query = new LgTour();
		$options['order_by']='datum DESC';
		$options['condition']= "datum < '" . date("Y-m-d") . "'";
		$query->setHidden(0);
		
		$cursor=$query->queryByExample($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		while ($result=$cursor->next()){
			$tourList[]=$result;
		}		
		
		$smarty->assign("menue_item","tourenarchiv");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Tourenarchiv");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("tourlist",$tourList);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());				
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);
		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_tourenarchiv.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");

		return true;
	}
	
	function getTourenarchivDetailPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		// Tour laden
		$tourDetail = array();

		$options['condition'] = "OID = '" . $i_form['OID'] . "' ";
				
		$query = new LgTour();
		$cursor=$query->queryByExample($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		while ($result=$cursor->next()){
			$tourDetail[]=$result;
		}				

		$query = new LgTour();
		$cursor=$query->queryByExample($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		$tour=$cursor->next();
		
//		$bilderParken=array();
//		$bilderTreff=array();
		$fotoimpressionen=array();
		
//		$query=new LgBild();
//		$query->setTour($tour);
//		$query->setParkmoeglichkeit(1);
//		
//		$cursor=$query->queryByExample();
//		while ($result=$cursor->next()) {
//			$bilderParken[]=$result;
//		}
		
//		$query=new LgBild();
//		$query->setTour($tour);
//		$query->setTreffpunkt(1);
//		$cursor=$query->queryByExample();
//		while ($result=$cursor->next()) {
//			$bilderTreff[]=$result;
//		}

		$query = new LgBild();
		$options = '';
		$options['order_by']='sortierung ASC';
		
		$query->setTour($tour);

		$cursor=$query->queryByExample($options);
		
		while ($result=$cursor->next()) {
			$fotoimpressionen[]=$result;
		}		
		$smarty->assign("fotoimpressionen",$fotoimpressionen);

		$smarty->assign("bilderparken",$bilderParken);
		$smarty->assign("bildertreff",$bilderTreff);		
		
		$smarty->assign("menue_item","tourenarchiv_detail");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Tourenarchiv");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("tourdetail",$tourDetail);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());	
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());			
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_tourenarchiv_detail.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");

		return true;
	}
	
	function getAnfragenFormPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		$smarty->assign("menue_item","anfragen");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Anfrage");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());	
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());			
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_anfragen_form.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");

		return true;
	}	
	
	function getAnfragenFormCommitPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		$smarty->assign("menue_item","anfragen");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Anfrage");
		
		// ggf. Formular verarbeiten
		$errors = array();
		if($i_form['anfrage_formular_name'] == '') {
			$errors['name'] = true;
		}
		
		if(($i_form['anfrage_formular_email'] == '' || !strpos($i_form['anfrage_formular_email'], '@')) &&  
			$i_form['anfrage_formular_telefon'] == '') {
			$errors['email'] = true;
			$errors['telefon'] = true;
		}
		
		$captcha = $controller->captcha();
					
		if(!$captcha->isCaptcha($i_form['anfrage_formular_sicherheitscode'])) {
			$errors['sicherheitscode'] = true;	
		}			
		
		// wenn die Eingaben fehlerhaft waren wird auf die Eingabeseite verwiesen, 
		// anderenfalls, wird die E-Mail versendet und eine Erfolsmeldung ausgegeben.
		if (sizeof($errors)){
			
			$errors['hinweis'] =  '<p class="fehler">Bitte f&uuml;lle die rot markierten Felder korrekt aus.</p>';
			$i_form['errors'] = $errors;
			
			return UiApplication::getAnfragenFormPage($i_form);
		} else {
			UiApplication::sendAnfragenMail($i_form);

			$smarty->assign("form",$i_form);
			$smarty->assign("rotation",UiApplication::getBildrotation());
			$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
			$smarty->assign("link_weitere",UiApplication::getLinkWeitere());						
//			$smarty->assign("tourheader",UiApplication::getAktuelleTour());
			$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
			$smarty->assign("email_body",$CFG->weiterempfehlen['body']);			
		
			$smarty->assign("hauptmenue","fe_hauptmenue.html");
			$smarty->assign("content","fe_anfragen_form_commit.html");
			$smarty->assign("headertop","fe_headertop.html");
			$smarty->assign("bilderrotation","fe_bildrotation.html");
			$smarty->assign("teaser","fe_teaser.html");
			$smarty->assign("seitenbild","fe_seitenbild.html");
			$smarty->assign("footermenue","fe_footermenue.html");
			return true;
		}
	}
		
	function getKontaktFormPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		$smarty->assign("menue_item","kontakt");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Kontakt");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());				
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_kontakt_form.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");

		return true;
	}
	
	function getKontaktFormCommitPage(&$i_form){
		global $CFG;		
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		$smarty->assign("menue_item","kontakt");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Kontakt");
		
		// ggf. Formular verarbeiten
		$errors = array();
		if($i_form['kontakt_formular_name'] == '') {
			$errors['name'] = true;
		}
		
		if(($i_form['kontakt_formular_email'] == '' || !strpos($i_form['kontakt_formular_email'], '@')) &&  
			$i_form['kontakt_formular_telefon'] == '') {
			$errors['email'] = true;
			$errors['telefon'] = true;
		}
		
		$captcha = $controller->captcha();
					
		if(!$captcha->isCaptcha($i_form['kontakt_formular_sicherheitscode'])) {
			$errors['sicherheitscode'] = true;	
		}			
		
		// wenn die Eingaben fehlerhaft waren wird auf die Eingabeseite verwiesen, 
		// anderenfalls, wird die E-Mail versendet und eine Erfolsmeldung ausgegeben.
		if (sizeof($errors)){
			
			$errors['hinweis'] =  '<p class="fehler">Bitte f&uuml;lle die rot markierten Felder korrekt aus.</p>';
			$i_form['errors'] = $errors;
			
			return UiApplication::getKontaktFormPage($i_form);
		} else {
			UiApplication::sendKontaktMail($i_form);

			$smarty->assign("form",$i_form);
			$smarty->assign("rotation",UiApplication::getBildrotation());
			$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());	
			$smarty->assign("link_weitere",UiApplication::getLinkWeitere());					
//			$smarty->assign("tourheader",UiApplication::getAktuelleTour());
			$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
			$smarty->assign("email_body",$CFG->weiterempfehlen['body']);			
		
			$smarty->assign("hauptmenue","fe_hauptmenue.html");
			$smarty->assign("content","fe_kontakt_form_commit.html");
			$smarty->assign("headertop","fe_headertop.html");
			$smarty->assign("bilderrotation","fe_bildrotation.html");
			$smarty->assign("teaser","fe_teaser.html");
			$smarty->assign("seitenbild","fe_seitenbild.html");
			$smarty->assign("footermenue","fe_footermenue.html");
			return true;
		}
	}
	
	function getImpressumPage(){
		global $CFG;
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();
		
		$smarty->assign("email_gutelaunewalker",$CFG->email['gutelaunewalker']);
		
		$smarty->assign("menue_item","impressum");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Impressum");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("rotation",UiApplication::getBildrotation());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());	
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());			
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_impressum.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");

		return true;
	}

	function getLinkPage(){
		global $CFG;
		$smarty		= &Ui::smarty();
		$controller	= &Ui::controller();

		
		$smarty->assign("menue_item","link");
		$smarty->assign("title_tag","GuteLauneWalkerBerlin - Weitere Links");
		
		$smarty->assign("form",$i_form);
		$smarty->assign("rotation",UiApplication::getBildrotation());
//		$smarty->assign("tourheader",UiApplication::getAktuelleTour());
		$smarty->assign("email_subject",$CFG->weiterempfehlen['subject']);
		$smarty->assign("email_body",$CFG->weiterempfehlen['body']);		
		$smarty->assign("hauptmenue","fe_hauptmenue.html");
		$smarty->assign("content","fe_link.html");
		$smarty->assign("headertop","fe_headertop.html");
		$smarty->assign("bilderrotation","fe_bildrotation.html");
		$smarty->assign("teaser","fe_teaser.html");
		$smarty->assign("seitenbild","fe_seitenbild.html");
		$smarty->assign("footermenue","fe_footermenue.html");
		
		$smarty->assign("link_weitere",UiApplication::getLinkWeitere());
		$smarty->assign("link_ausgewaehlt",UiApplication::getLinkAusgewaehlt());
		
		return true;
	}
	
	/**
	 * Versendet die E-Mail des Kontaktformulars
	 * 
	 * @param Array ($i_form)
	 * @return Array (ErrorList)
	 */
	function sendKontaktMail(&$i_form){
		global $CFG;

		$mail = new PHPMailer();
		$mail->IsHTML(false);
		$mail->From			= $CFG->kontaktformular['email'];
		$mail->FromName		= $CFG->kontaktformular['name'];
		$mail->Subject		= $CFG->kontaktformular['betreff'];
		$mail->AddAddress($CFG->kontaktformular['email']);
		$mail->AddBCC('testing@vipermedia.de');
			
		$tourankuendigung = ($i_form['kontakt_formular_tourenankuendigung']==0?"nicht ":"");
		
		$mail->Body  = "Es wurde eine Kontaktanfrage " . iconv('utf-8','iso-8859-1', 'über') . " die Webseite eingereicht.\n\n";
		
		$mail->Body .= "Name: " . iconv('utf-8','iso-8859-1', $i_form['kontakt_formular_name']) . " \n";
		
		$mail->Body .= "E-Mail-Adresse: " . iconv('utf-8','iso-8859-1', $i_form['kontakt_formular_email']) . " \n \n";
		$mail->Body .= "Telefon: ".$i_form['kontakt_formular_telefon']." \n \n";
		
		$mail->Body .= "Walkerfreund " . iconv('utf-8','iso-8859-1', $i_form['kontakt_formular_name']) . " " . iconv('utf-8','iso-8859-1', 'möchte') . " " .$tourankuendigung. "in den Verteiler der " . iconv('utf-8','iso-8859-1', 'Tourankündigungen') . " aufgenommen werden. \n \n";

		$mail->Body .= "\nDie Nachricht lautet: \n\n";
		$mail->Body .= "----- \n ";
		$mail->Body .= iconv('utf-8','iso-8859-1', $i_form['kontakt_formular_nachricht']) . " \n";
		$mail->Body .= "----- \n ";
		$mail->Body .= "Diese E-Mail wurde automatisch erstellt. \n";
		
		$mail->Send();

	}	
	
	/**
	 * Versendet die E-Mail des Anfrageformulars
	 * 
	 * @param Array ($i_form)
	 * @return Array (ErrorList)
	 */
	function sendAnfragenMail(&$i_form){
		global $CFG;

		$mail = new PHPMailer();
		$mail->IsHTML(false);
		$mail->From			= $CFG->anfrageformular['email'];
		$mail->FromName		= $CFG->anfrageformular['name'];
		$mail->Subject		= $CFG->anfrageformular['betreff'];
		$mail->AddAddress($CFG->anfrageformular['email']);
		$mail->AddBCC('testing@vipermedia.de');
			
		$tourankuendigung = ($i_form['anfrage_formular_tourenankuendigung']==0?"nicht ":"");
		
		$mail->Body  = "Es wurde eine Anfrage " . iconv('utf-8','iso-8859-1', 'über') . " die Webseite eingereicht.\n\n";
		
		$mail->Body .= "Name: " . iconv('utf-8','iso-8859-1', $i_form['anfrage_formular_name']) . " \n";
		
		$mail->Body .= "E-Mail-Adresse: " . iconv('utf-8','iso-8859-1', $i_form['anfrage_formular_email']) . " \n \n";
		$mail->Body .= "Telefon: ".$i_form['anfrage_formular_telefon']." \n \n";
		
		$mail->Body .= "Walkerfreund " . iconv('utf-8','iso-8859-1', $i_form['anfrage_formular_name']) . " " . iconv('utf-8','iso-8859-1', 'möchte') . " " .$tourankuendigung. "in den Verteiler der " . iconv('utf-8','iso-8859-1', 'Tourankündigungen') . " aufgenommen werden. \n \n";

		$mail->Body .= "\nDie Nachricht lautet: \n\n";
		$mail->Body .= "----- \n ";
		$mail->Body .= iconv('utf-8','iso-8859-1', $i_form['anfrage_formular_nachricht']) . " \n";
		$mail->Body .= "----- \n ";
		$mail->Body .= "Diese E-Mail wurde automatisch erstellt. \n";
		
		$mail->Send();

	}	
	
	/**
	 * Versendet die E-Mail der Anmeldung zu einer Tour
	 * 
	 * @param Array ($i_form)
	 * @return Array (ErrorList)
	 */
	function sendTourAnmeldungMail(&$i_form){
		global $CFG;

		$mail = new PHPMailer();
		$mail->IsHTML(false);
		$mail->From			= $CFG->anmeldungformular['email'];
		$mail->FromName		= $CFG->anmeldungformular['name'];
		$mail->Subject		= 'Anmeldung zur Tour ' . $i_form['aktuelle_tour_name'] . ' am ' . $i_form['aktuelle_tour_datum'] . '';
		$mail->AddAddress($CFG->anmeldungformular['email']);
		$mail->AddBCC('testing@vipermedia.de');
			
		$freunde = ($i_form['aktuelle_tour_formular_freunde']==''?"keine":$i_form['aktuelle_tour_formular_freunde']);	
		$stoecke = ($i_form['aktuelle_tour_formular_stoecke']==''?"kein":$i_form['aktuelle_tour_formular_stoecke']);
		$tourankuendigung = ($i_form['aktuelle_tour_formular_tourenankuendigung']==0?"nicht ":"");
		
		$mail->Body  = "Es wurde eine Anmeldung " . iconv('utf-8','iso-8859-1', 'für') . " die Tour " . iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_name']) . " am " . $i_form['aktuelle_tour_datum'] . " eingereicht.\n\n";
		
		$mail->Body .= "Name: " . iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_name']) . " \n";
		$mail->Body .= "Vorname: " . iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_vorname']) . " \n";
		
		$mail->Body .= "E-Mail-Adresse: " . iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_email']) . " \n \n";
		$mail->Body .= "Telefon: " . $i_form['aktuelle_tour_formular_telefon'] . " \n \n";
		
		$mail->Body .= iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_vorname']) . " " .iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_name']) . " " . iconv('utf-8','iso-8859-1', 'möchte') . " ".$stoecke." Paar Nordic Walking " . iconv('utf-8','iso-8859-1', 'Stöcke') . " ausleihen. \n \n";		
		$mail->Body .= iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_vorname']) . " " . iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_name']) . " bringt ".$freunde." Freunde mit. \n \n";
		
		$mail->Body .= iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_vorname']) . " "  . iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_name']) . " " . iconv('utf-8','iso-8859-1', 'möchte') . " " .$tourankuendigung. "in den Verteiler der " . iconv('utf-8','iso-8859-1', 'Tourankündigungen') . " aufgenommen werden. \n \n";

		$mail->Body .= "\nDie Nachricht lautet: \n\n";
		$mail->Body .= "----- \n ";
		$mail->Body .= iconv('utf-8','iso-8859-1', $i_form['aktuelle_tour_formular_nachricht']) . " \n";
		$mail->Body .= "----- \n ";
		$mail->Body .= "Diese E-Mail wurde automatisch erstellt. \n";
		
		$mail->Send();
	}	

	/**
	 * liefert die Bilder fuer die Rotation
	 * 
	 * @param 
	 * @return Array ($bilderRotation)
	 */	
	function getBildrotation () {

		$bilderRotation = array();
		
		$query = new LgBild();
		$tourjoin = new LgTour();

		$tourjoin->setHidden(0);
		$query->setBildrotation(1);
		
		$query->addJoin($tourjoin, 'fk');
		
		$options = array();
		
		$options['order_by'] = 'OID DESC' ;
		
		$cursor=$query->queryByExample($options);
		while ($result=$cursor->next()) {
			$bilderRotation[]=$result;
		}		
		return $bilderRotation;
	}

	/**
	 * liefert die aktuelle Tour 
	 * 
	 * @param 
	 * @return Array ($aktuelleTour)
	 */	
	function getAktuelleTour () {
		// Tour laden
		$query = new LgTour();
		
		if($_GET['OID'] && $_GET['hidden']) {
			$query->setOID($_GET['OID']);
			//$query->setHidden(1);
		} else {
			$options['order_by'] = 'datum DESC';
			$options['offset'] = '0'; 
			$options['count'] = '1'; 
			$options['condition'] = "datum >= '" . date("Y-m-d") . "'";
			$query->setHidden(0);			
		}
		$cursor=$query->queryByExample($options);
		if(PEAR::isError($cursor) ){
			die ($cursor->getMessage());
		}
		$aktuelleTour=$cursor->next();
		return $aktuelleTour;
	}	
	
	/**
	 * Liefert eine Bilddatei aus
	 *
	 * @param unknown_type $i_form
	 */
	function getImageFile(&$i_form){
		
		$query = new LgBild();
		//$query->setTour($i_form['OID']);
		
		$bild = $query->selectByOid($i_form['OID']);
		
		if(!PEAR::isError($bild)){
			$filename = $bild->getThumbnailFilename($i_form['width'], $i_form['height']);
	
			$fd=fopen ( $filename, "rb");
			if($fd && $bild->isImage()){
				header( "Content-Type: ".$bild->getMimeType() );
				header( "Content-Disposition: inline; filename=\"".$filename);
				header( "Content-length: ".(string)(filesize($filename)) );
				$fd=fopen($filename,'rb');
				while(!feof($fd)) {
					print fread($fd, 4096);
				}
				fclose($fd);
			}else{
				echo "Datei kann nicht geöffnet werden.";	
			}
		}else{
			echo "Datei nicht vorhanden.";	
		}
		exit;	
	}
	
	/**
	 * liefert das Captcha aus
	 * 
	 * @param $i_form
	 */
	function getCaptcha(&$i_form){
		$controller	= &Ui::controller();
		$capcha = $controller->captcha();
		$capcha->showCaptcha();
	}

	/**
	 * Liefert eine Liste der ausgewählten Links
	 *
	 * @param unknown_type $i_form
	 */
	function getLinkAusgewaehlt(){
		$read = new LgLink();
		return $read->readFileLinkAusgewaehlt();
	}

	/**
	 * Liefert eine Liste der weiteren Links
	 *
	 * @param unknown_type $i_form
	 */
	function getLinkWeitere(){
		$read = new LgLink();
		return $read->readFileLinkWeitere();
	}	
}

?>
