<?php

/**
 * Projekt:     VmFramework Demo-App
 * File:        Ui.class.php
 *
 * Beispielanwendung zur Demonstration der Architektur der Vipermedia-Tools
 *
 * @copyright 1999-2005 ViperMedia M.Neumann 6 T.Roediger GbR
 * @author Thomas Roediger <t.roediger@vipermedia.de>
 * @package VmFramework.Core
 */

// Smarty einbinden
define("SMARTY_DIR",$CFG->smarty_dir);
require( SMARTY_DIR . "Smarty.class.php" );

/**
 * UI ermitteltr die auszufuehrende Aktion, initialisiert die notwendigen 
 * Pakete und gibt die gewuenschte Seite zurueck
 *
 * @package VmFramework.Ui
 */
class Ui{

	var $datum;
	
	function processRequest(&$i_form){
		$CFG=Controller::config();
		$action=Ui::getAction($i_form);
		$page=$i_form["page"];
		$processor=Ui::getProcessor($page,$action);
		$smarty=&Ui::smarty();
		$smarty->assign("SID",session_id());
		$controller=&Controller::getInstance();


		$ok=false;
		list($class,$method)=explode("::",$processor);
		if ( strlen($class) && strlen($method) ) {
			eval("\$ok=$processor(\$i_form);");
		}
		
		if($ok){
			//$skin=&$controller->skin($i_form);
			//$smarty->assign("skin",$skin);
			$smarty->assign("portal",$portal);
			$smarty->assign("ERRORS",$GLOBALS['ERRORS']);
			$smarty->assign("WARNINGS",$GLOBALS['WARNINGS']);
			$smarty->display("fe_body.html");

		}
		return;
	}

	function getProcessor(&$i_page,$i_mode=""){
		$CFG=Controller::config();
		include( $CFG->ui_dir . "UiApplication.class.php" );
		switch($i_page){
			case "start":
				return "UiApplication::getStart_Page";
				break;
			case "aktuelle_tour_detail":
				return "UiApplication::getAktuelleTourDetailPage";
				break;
			case "aktuelle_tour_form":
				return "UiApplication::getAktuelleTourFormPage";
				break;
			case "aktuelle_tour_form_commit":
				return "UiApplication::getAktuelleTourFormCommitPage";
				break;
            case "kurse":
                return "UiApplication::getKursePage";
                break;
            case "fewo":
                return "UiApplication::getFewoPage";
                break;
			case "tourenarchiv":
				return "UiApplication::getTourenarchivPage";
				break;
			case "tourenarchiv_detail":
				return "UiApplication::getTourenarchivDetailPage";
				break;				
			case "anfragen_form":
				return "UiApplication::getAnfragenFormPage";
				break;
			case "anfragen_form_commit":
				return "UiApplication::getAnfragenFormCommitPage";
				break;				
			case "kontakt_form":
				return "UiApplication::getKontaktFormPage";
				break;
			case "kontakt_form_commit":
				return "UiApplication::getKontaktFormCommitPage";
				break;								
			case "impressum":
				return "UiApplication::getImpressumPage";
				break;
			case "link":
				return "UiApplication::getLinkPage";
				break;				
			case "imagefile":
				return "UiApplication::getImageFile";
				break;				
			case "captcha":
				return "UiApplication::getCaptcha";
				break;				
			default:
				$i_page="start";
				return "UiApplication::getStart_Page";
				break;
		}
	}
	
	function getAction(&$i_form){
		if( isset($i_form["action"])  ){
			return $i_form["action"];
		}elseif(isset($i_form["action_start"])){
			$i_form["page"]="start";
		}elseif(isset($i_form["action_aktuelle_tour_detail"])){
			$i_form["page"]="aktuelle_tour_detail";
		}elseif(isset($i_form["action_aktuelle_tour_form"])){
			$i_form["page"]="aktuelle_tour_form";			
		}elseif(isset($i_form["action_aktuelle_tour_form_commit"])){
			$i_form["page"]="aktuelle_tour_form_commit";
        }elseif(isset($i_form["action_kurse"])){
            $i_form["page"]="kurse";
        }elseif(isset($i_form["action_fewo"])){
            $i_form["page"]="fewo";
		}elseif(isset($i_form["action_tourenarchiv"])){
			$i_form["page"]="tourenarchiv";
		}elseif(isset($i_form["action_tourenarchiv_detail"])){
			$i_form["page"]="tourenarchiv_detail";						
		}elseif(isset($i_form["action_anfragen_form"])){
			$i_form["page"]="anfragen_form";
		}elseif(isset($i_form["action_anfragen_form_commit"])){
			$i_form["page"]="anfragen_form_commit";
		}elseif(isset($i_form["action_kontakt_form"])){
			$i_form["page"]="kontakt_form";
		}elseif(isset($i_form["action_kontakt_form_commit"])){
			$i_form["page"]="kontakt_form_commit";			
		}elseif(isset($i_form["action_impressum"])){
			$i_form["page"]="impressum";
		}elseif(isset($i_form["action_link"])){
			$i_form["page"]="link";			
		}elseif( isset($i_form["action_imagefile"]) ){
			$i_form['page']='imagefile';
			return "imagefile";			
		}elseif( isset($i_form["action_captcha"]) ){
			$i_form['page']='captcha';
			return "captcha";			
		}else{
			$i_form["page"]="start";
		}
		return "";
		
	}
	
	function &controller(){
		return Controller::getInstance();
	}
	
	function &smarty(){
		global $CFG;
		static $smarty=null;
		if(!$smarty){
			$smarty=new Smarty();
			$smarty->template_dir=$CFG->smarty_template_dir;
			$smarty->compile_dir=$CFG->smarty_compile_dir;
			$smarty->config_dir = $CFG->smarty_config_dir;
			$smarty->debugging=$CFG->smarty_debug;
			$smarty->debug_tpl=$CFG->smarty_debug_tpl;
		}
		return $smarty;
	}

}
?>