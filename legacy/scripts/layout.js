// Konfiguration der ID's,
// welche auf eine Höhe gebracht werden sollen
rel_id = new Array();
rel_id[0] = 'content';
rel_id[1] = 'teaser';
rel_id[2] = 'left';

var max_hoehe = 0;
var nn = '' ;
function setDivHeight(pxOffset) {
  // return true;
  // Prüfung, ob alle konfigurierten ID auch tatsächlich im HTML vorkommen
  // gleichzeitig ermitteln der Offset-Höhe,
  // um später die ID auf den gemeinsamen höchsten Wert stellen
  for (var i = 0; i < rel_id . length; i++) {
    if (!document.getElementById(rel_id[i])) {
      return true;
    } else {
      var hoehevals = document . getElementById(rel_id[i]) . offsetHeight;
      nn = nn + ' ' + hoehevals;
      if (hoehevals > max_hoehe) {
        max_hoehe = hoehevals;
      }
    }
  }
  // max_hoehe = '' + max_hoehe/10 + 'em';
  max_hoehe = '' + (max_hoehe + 10) + 'px';
  for (var i = 0; i < rel_id . length; i++) {
    setHeight(rel_id[i], max_hoehe);
  }
}

function setHeight(elem, hoehe) {
  document.getElementById(elem).style.height = hoehe;
}

function init(func) {
  if (1) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
      window.onload = func;
    }
  }
}

init(
  function() {
    setDivHeight(0);
  }
);
