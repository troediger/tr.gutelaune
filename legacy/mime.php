<?
	$mimetypes=array(
						"zip"	=> "application/zip",
						"gif"	=> "image/gif",
						"jpeg"	=> "image/jpeg",
						"jpe"	=> "image/jpeg",
						"jpg"	=> "image/jpeg",
						"JPG"	=> "image/jpeg",
						"png"	=> "image/png",
						"tif"	=> "image/tiff",
						"tiff"	=> "image/tiff",
						
						"csv"	=> "text/comma-separated-values",
						"doc"	=> "application/msword",
						"dot"	=> "application/msword",
						"mdb"	=> "application/msaccess",
						"ps"	=> "application/postscript",
						"eps"	=> "application/postscript",
						"ai"	=> "application/postscript",
						"pdf"	=> "application/pdf",
						
						"asf"	=> "video/x-ms-asf",
						"asx"	=> "video/x-ms-asf",
						"wmv"	=> "video/x-ms-wmv",
						"mpeg"	=> "video/mpeg",	
						"mpg"	=> "video/mpeg",
						"mpe"	=> "video/mpeg",	
						"qt"	=> "video/quicktime", 	
						"mov"	=> "video/quicktime", 	
						"viv"	=> "video/vnd.vivo",	
						"vivo"	=> "video/vnd.vivo",
						"avi"	=> "video/x-msvideo", 	
						"movie"	=> "video/x-sgi-movie" 
						
					);
?>