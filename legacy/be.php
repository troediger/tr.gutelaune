<?php

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") ." GMT");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Content-Type: text/html; charset=UTF-8");
require_once("libs/pear/PEAR.php");

require("config.php" );

ini_set("magic_quotes_gpc","1");
ini_set("session.name","SID");
ini_set("register_globals","off");
ini_set("include_path",$CFG->include);

//include_once($CFG->fckeditor_dir."fckeditor.php") ;

require("Benchmark/Timer.php");
$timer =& new Benchmark_Timer(false);

error_reporting  (E_ERROR | E_WARNING  | E_PARSE);
//error_reporting  (E_ALL);

define("PORA_DIR",$CFG->pora_dir);

require( PORA_DIR . "DbService.class.php" );

require("ui/UiBe.class.php");
require("core/Controller.class.php");
Controller::loadClasses();

session_start();
$form = $_POST ? $_POST : $_GET ;
$ERRORS=array();
$WARNINGS=array();
Controller::connect();

$timer->setMarker("Intialisation done");
UiBe::processRequest($form);
//DbService::closeDb();
foreach($CFG->pora_tables as $key=>$value)
	DbService::closeDb($key);

flush();


?>