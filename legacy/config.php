<?PHP
/*
In config.php werden alle Konfigurationsvariablen eingestellt.
*/
class Object{};
// $CFG enhaelt alle Applikationsweiten Konfigurationen
$CFG=new Object();


// Betriebsart
// local || live - local=default in den switch-anweisungen
//$CFG->mode='local';
//$CFG->mode='live';
$CFG->mode = ((strpos($_SERVER['SERVER_NAME'],'vipermedia')===false))?'live':'local';

// switch-daten
switch($CFG->mode){
	
	case'live':
	$CFG->pora_connections=array(
		'default'=>array(
			'dsn'=>'mysql://guteql_1:Ac99tdr8@sql40.your-server.de/guteql_db1',
			'charset_name'=>'utf8',				// utf8 || latin1
			'collation_name'=>'utf8_unicode_ci',	// utf8_unicode_ci || latin1_swedish_ci
		)
	);
	$CFG->pora_strict=false;
	$CFG->dpi	= 96;
	$CFG->email = array(
		'schnupperkurse'=>'schnupperkurse@gutelaunewalkerberlin.de',
		'fitindenfruehling'=>'fitindenfruehling@gutelaunewalkerberlin.de',
		'frischeluft'=>'frischeluft@gutelaunewalkerberlin.de',
		'gutelaunewalker'=>'gutelaunewalkerberlin@gmx.de');
	$CFG->anmeldungformular = array(
		'email'=>'rsmu@gmx.de',
		'name'=>'');	
	$CFG->kontaktformular = array(
		'email'=>'rsmu@gmx.de',
		'name'=>'',
		'betreff'=>'Kontakt-Mail GuteLauneWalkerBerlin.de');
	$CFG->anfrageformular = array(
		'email'=>'rsmu@gmx.de',
		'name'=>'',
		'betreff'=>'Anfrage-Mail GuteLauneWalkerBerlin.de');	
	$CFG->weiterempfehlen = array(
		'subject'=>'Websiteempfehlung GuteLauneWalkerBerlin.de',
		'body'=>'Hallo,%0A
ich möchte Dir die Webseite www.GuteLauneWalkerBerlin.de empfehlen.%0A
Sylvia, Rainer und Max bieten wechselnde Nordic Walking Touren in und um%0A
Berlin für Jedermann an. Vieleicht wäre das ja auch was für Dich?%0A
Viele Grüße!');			
	break;


	default:
	$CFG->pora_connections=array(
		'default'=>array(
			'dsn'=>'mysql://root:R0SEBu0e@localhost/gutelaunewalker',
			'charset_name'=>'utf8',				// utf8 || latin1
			'collation_name'=>'utf8_unicode_ci',	// utf8_unicode_ci || latin1_swedish_ci
		)
	);
	$CFG->pora_strict=true;
	$CFG->dpi	= 72;
	$CFG->email = array(
		'schnupperkurse'=>'schnupperkurse@gutelaunewalkerberlin.de',
		'fitindenfruehling'=>'fitindenfruehling@gutelaunewalkerberlin.de',
		'frischeluft'=>'frischeluft@gutelaunewalkerberlin.de',
		'gutelaunewalker'=>'gutelaunewalkerberlin@gmx.de');
	$CFG->anmeldungformular = array(
		'email'=>'testing@vipermedia.de',
		'name'=>'');	
	$CFG->kontaktformular = array(
		'email'=>'testing@vipermedia.de',
		'name'=>'',
		'betreff'=>'Kontakt-Mail GuteLauneWalkerBerlin.de');
	$CFG->anfrageformular = array(
		'email'=>'testing@vipermedia.de',
		'name'=>'',
		'betreff'=>'Anfrage-Mail GuteLauneWalkerBerlin.de');
	$CFG->weiterempfehlen = array(
		'subject'=>'Websiteempfehlung GuteLauneWalkerBerlin.de',
		'body'=>'Hallo,%0A
ich möchte Dir die Webseite www.GuteLauneWalkerBerlin.de empfehlen.%0A
Sylvia, Rainer und Max bieten wechselnde Nordic Walking Touren in und um%0A
Berlin für Jedermann an. Vieleicht wäre das ja auch was für Dich?%0A
Viele Grüße!');		
	break;
}


// Pfade, verwendete Bibliotheken und deren Konfigurationen
$CFG->base_dir=ereg("/$",$GLOBALS['_SERVER']['DOCUMENT_ROOT']) ? $GLOBALS['_SERVER']['DOCUMENT_ROOT'] : $GLOBALS['_SERVER']['DOCUMENT_ROOT'] . "/" ;
$CFG->cache_dir=$CFG->base_dir."cache/";
// Ado
$CFG->ado_dir=$CFG->base_dir."libs/adodb/";
$CFG->ado_driver_dir = $CFG->ado_dir . "drivers/";
$CFG->ado_debug=false;
$CFG->ado_case=2;
$CFG->ado_cache_dir=$CFG->cache_dir ."ado";

// PORA
$CFG->pora_tables=array(
//	'portal'=>'heizcheck',
//	'heiznebenkosten'=>'heizcheck',
//	'textblock'=>'moderat',
//	'foerderprogramm'=>'foerderrat',
//	'foerderprogramm_plz'=>'foerderrat',
);
$CFG->pora_dir="libs/pora.ado/";
$CFG->pora_deleted_flag="enabled";
$CFG->pora_deleted_true="n";
$CFG->pora_deleted_false="y";
$CFG->pora_use_cache=false;
$CFG->table_prefix="";
$CFG->type_map="mysql";
$CFG->generator_output_dir=$CFG->base_dir."db/";
//$CFG->pora_dblayer='adodb';

// Charsets
$CFG->html_charset ='utf-8';			// utf-8 || iso-8859-1

// Smarty-Template-Engine
$CFG->smarty_dir="libs/Smarty/libs/";
$CFG->smarty_template_dir=$CFG->base_dir."templates/";
$CFG->smarty_config_dir= $CFG->smarty_template_dir . "configs/";
$CFG->smarty_compile_dir=$CFG->cache_dir . "templates_c/";
$CFG->smarty_debug=false;
$CFG->smarty_debug_tpl='debug.tpl';

// wo liegen die Dateien fuer die Erzeugung der Oberflaeche?
$CFG->ui_dir="ui/";

$CFG->img_dir="upload/";

$CFG->data_dir="data/";
$CFG->datei_link_weitere = "link_weitere.txt";
$CFG->datei_link_ausgewaehlt = "link_ausgewaehlt.txt";

// path for thumbnails
$CFG->thumbnail_path = 'cache/thumbs/';

// welches ist das standard-portal?
//$CFG->default_tour_id="test";
$CFG->be_num_rows=20;

if(OS_WINDOWS){
	$CFG->include=".;./libs/pear;$CFG->ado_dir;$CFG->ado_driver_dir";
}else{
	$CFG->include=".:./libs/pear:$CFG->ado_dir:$CFG->ado_driver_dir";
}

// Definition der Seitennamen fuer Statistik
$CFG->pages=array(
	"VISIT"=>"Gesamtbesucher",
	"start"=>"Startseite",
	"insertdata"=>"Daten anlegen",
	"deletedata"=>"Daten löschen",
);


?>