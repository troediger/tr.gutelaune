<?php

// Cache-sichere Header
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") ." GMT");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
// einbinden von PEAR
require_once("libs/pear/PEAR.php");

// einbinden der Konfigurationsdaten
require("config.php" );

// PHP-Mailer
require_once("libs/class.phpmailer.php");

// PHP-Konfigurations-Variablen-setzten
ini_set("magic_quotes_gpc","1");
ini_set("session.name","SID");
ini_set("register_globals","off");
ini_set("include_path",$CFG->include);

// Einbinden der PEAR-Benchmark-Bibliothek
require("Benchmark/Timer.php");

// initialisieren des Benchmark, wenn der Konstrultionsparameter true ist,
// wird automatisch die Statistik ausgegeben
$timer =& new Benchmark_Timer(false);

// Error-Reporting setzten
error_reporting  (E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR);

// PORA-Verzeichnis setzen
define("PORA_DIR",$CFG->pora_dir);

// DBService einbinden
require( PORA_DIR . "DbService.class.php" );

// UI einbinden
require("ui/Ui.class.php");

// Controller einbinden
require("core/Controller.class.php");

// benoetigte Core-klassen laden lassen
Controller::loadClasses();

// Session starten
session_start();

// Datenbankverbindung herstellen
Controller::connect();

// Marker im Benchmark setzten
$timer->setMarker("Initialisierung fertig.");

// Benutzeranfrage verarbeiten
Ui::processRequest($_REQUEST);

DbService::closeDb();
foreach($CFG->pora_tables as $key=>$value)
	DbService::closeDb($key);
flush();

?>